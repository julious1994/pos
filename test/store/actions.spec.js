import expect from 'expect';
import * as actions from '../../app/store/actions';
import * as types from '../../app/store/action.types';

describe('actions', () => {
  it('should create an action app set', () => {
    expect(actions.appSet('local store')).toEqual({
      type: types.APP_SET,
      payload: 'local store',
    });
  });
});
