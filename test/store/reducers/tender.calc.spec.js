import expect from 'expect';
import reducer from '../../../app/store/reducers/tender';
import * as actions from '../../../app/store/actions';
import { InitialState } from '../../../app/store/initial.state';

describe('reducer tender calc (cart payment)', () => {
  const initState = InitialState.cart.tabs[0];

  it('Payment : should handle TENDER_CALC_PRESS', () => {
    const newTender = { value: 100, method: 'CASH (USD)' };
    const tenders = [newTender];
    const calc = { init: true };
    const expectedState = Object.assign({}, initState, {
      payment: Object.assign({}, initState.payment, {
        tenders,
        selectedTender: 0,
        calc,
      }),
    });

    let state = reducer(
      initState,
      actions.addTender(newTender.value, newTender.method)
    );
    state = reducer(
      state,
      actions.pressTenderCalc('C')
    );

    tenders[0].value = '0';
    calc.init = false;
    expect(state).toEqual(expectedState);


    calc.init = true;
    tenders[0].value = '1';
    state = reducer(
      state,
      actions.pressTenderCalc('1')
    );
    expect(state).toEqual(expectedState);
    state = reducer(
      state,
      actions.pressTenderCalc('2')
    );
    state = reducer(
      state,
      actions.pressTenderCalc('3')
    );
    state = reducer(
      state,
      actions.pressTenderCalc('.')
    );
    state = reducer(
      state,
      actions.pressTenderCalc('4')
    );
    state = reducer(
      state,
      actions.pressTenderCalc('5')
    );

    tenders[0].value = '123.45';
    expect(state).toEqual(expectedState);

    state = reducer(
      state,
      actions.pressTenderCalc('+50')
    );
    state = reducer(
      state,
      actions.pressTenderCalc('+10')
    );
    state = reducer(
      state,
      actions.pressTenderCalc('+20')
    );
    tenders[0].value = '203.45';
    expect(state).toEqual(expectedState);

    state = reducer(
      state,
      actions.pressTenderCalc('DEL')
    );
    tenders[0].value = '203.4';
    expect(state).toEqual(expectedState);
  });
});
