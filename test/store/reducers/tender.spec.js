import expect from 'expect';
import reducer from '../../../app/store/reducers/tender';
import * as actions from '../../../app/store/actions';
import { InitialState } from '../../../app/store/initial.state';

describe('reducer tender (cart payment)', () => {
  const initState = InitialState.cart.tabs[0];

  it('Payment : should handle ADD_TENDER', () => {
    const newTender = { value: '0', method: 'CASH (USD)' };
    const expectedState = Object.assign({}, initState, {
      payment: Object.assign({}, initState.payment, {
        tenders: [newTender],
        selectedTender: 0,
      }),
    });

    const state = reducer(
      initState,
      actions.addTender(newTender.value, newTender.method)
    );
    expect(state).toEqual(expectedState);
  });

  it('Payment : should handle UPDATE_TENDER', () => {
    const newTender = { value: '560', method: 'CASH (USD)' };
    const expectedState = Object.assign({}, initState, {
      payment: Object.assign({}, initState.payment, {
        tenders: [newTender],
        selectedTender: 0,
      }),
    });

    const state = reducer(
      initState,
      actions.addTender(newTender.value, newTender.method)
    );

    const state2 = reducer(
      state,
      actions.updateTender(0, '560')
    );
    expect(state2).toEqual(expectedState);
  });

  it('Payment : should handle REMOVE_TENDER', () => {
    const newTender2 = { value: '90', method: 'CASH (USD)' };
    const expectedState = Object.assign({}, initState, {
      payment: Object.assign({}, initState.payment, {
        tenders: [newTender2],
        selectedTender: 1,
      }),
    });

    const state = reducer(
      initState,
      actions.addTender('50', 'CASH (USD)')
    );
    const state2 = reducer(
      state,
      actions.addTender('90', 'CASH (USD)')
    );
    const state3 = reducer(
      state2,
      actions.removeTender(0)
    );
    expect(state3).toEqual(expectedState);
  });

  it('Payment : should handle SELECT_TENDER', () => {
    const newTender = { value: '50', method: 'CASH (USD)' };
    const newTender2 = { value: '90', method: 'CASH (USD)' };
    const expectedState = Object.assign({}, initState, {
      payment: Object.assign({}, initState.payment, {
        tenders: [newTender, newTender2],
        selectedTender: 0,
      }),
    });

    const state = reducer(
      initState,
      actions.addTender('50', 'CASH (USD)')
    );
    const state2 = reducer(
      state,
      actions.addTender('90', 'CASH (USD)')
    );
    const state3 = reducer(
      state2,
      actions.selectTender(0)
    );
    expect(state3).toEqual(expectedState);
  });
});
