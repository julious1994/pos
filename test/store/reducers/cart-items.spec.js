import expect from 'expect';
import reducer, { findItemById, generateNewCartItem } from '../../../app/store/reducers/cart-items';
import * as actions from '../../../app/store/actions';
import { products } from '../../data';
import { InitialState } from '../../../app/store/initial.state';

describe('reducer cart-items', () => {
  it('Cart : should return on initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      InitialState.cart.tabs[0]
    );
  });

  const [newItem1, newItem2, newItem3] = products;

  const outcomeItem1 = generateNewCartItem({ item: newItem1 }, 0);
  const outcomeItem2 = generateNewCartItem({ item: newItem2 }, 1);
  const outcomeItem3 = generateNewCartItem({ item: newItem3 }, 2);

  const initCart = {
    items: [outcomeItem1, outcomeItem2],
    selectedItem: outcomeItem2,
    lastItemId: 2,
  };

  it('Cart : should handle ADD_ITEM, last added must selected', () => {
    let cart = initCart;
    const newOutcomeItem = outcomeItem3;

    const expectedCart = {
      items: [...cart.items, newOutcomeItem],
      selectedItem: newOutcomeItem,
      lastItemId: 3,
    };

    cart = reducer(
      cart,
      actions.addItem(newItem3)
    );

    expect(cart).toEqual(expectedCart);
  });


  it('Cart : should handle ADD_ITEM with same item, Increment Quantity with same interval', () => {
    let cart = initCart;
    const item1 = Object.assign({}, outcomeItem1);
    item1.qty = item1.interval * 2;

    const expectedCart = {
      items: [item1, outcomeItem2],
      selectedItem: item1,
      lastItemId: cart.lastItemId,
    };

    cart = reducer(
      cart,
      actions.addItem(newItem1)
    );

    expect(cart).toEqual(expectedCart);
  });

  it('Cart : should handle ADD_ITEM (new), marked it selected, last item id should updated', () => {
    let cart = initCart;

    const expectedCart = {
      items: [...cart.items, outcomeItem3],
      selectedItem: outcomeItem3,
      lastItemId: cart.lastItemId + 1,
    };

    cart = reducer(
      cart,
      actions.addItem(newItem3)
    );

    expect(cart).toEqual(expectedCart);
  });


  it('Cart : should handle SELECT_ITEM', () => {
    let cart = initCart;

    const selectItemId = 1;

    const expectedCart = Object.assign({}, cart, {
      selectedItem: findItemById(cart.items, selectItemId),
    });

    cart = reducer(
      cart,
      actions.selectItem(selectItemId)
    );

    expect(cart).toEqual(expectedCart);
  });

  it('Cart : should handle REMOVE_ITEM, selected item', () => {
    let cart = initCart;
    const { items, lastItemId } = cart;

    const expectedCart = {
      items: items.filter(i => i.itemId !== cart.selectedItem.itemId),
      selectedItem: items[0],
      lastItemId,
    };

    cart = reducer(
      cart,
      actions.removeItem()
    );

    expect(cart).toEqual(expectedCart);
  });

  it('Cart : should handle SET_ITEM_QTY, specified value', () => {
    let cart = initCart;
    const selectedItem = Object.assign({}, cart.selectedItem);
    const newQty = 100;

    selectedItem.qty = newQty;

    const expectedCart = {
      items: [outcomeItem1, selectedItem],
      selectedItem,
      lastItemId: cart.lastItemId,
    };

    cart = reducer(
      cart,
      actions.setItemQty(newQty)
    );

    expect(cart).toEqual(expectedCart);
  });

  it('Cart : should handle SET_ITEM_PRICE, specified value', () => {
    let cart = initCart;
    const selectedItem = Object.assign({}, cart.selectedItem);
    const newPrice = 100;

    selectedItem.rate = newPrice;

    const expectedCart = {
      items: [outcomeItem1, selectedItem],
      selectedItem,
      lastItemId: cart.lastItemId,
    };

    cart = reducer(
      cart,
      actions.setItemPrice(newPrice)
    );

    expect(cart).toEqual(expectedCart);
  });

  it('Cart : should handle SET_ITEM_DISC, specified value', () => {
    let cart = initCart;
    const selectedItem = Object.assign({}, cart.selectedItem);
    const newDisc = 55.23;

    selectedItem.disc = newDisc;

    const expectedCart = {
      items: [outcomeItem1, selectedItem],
      selectedItem,
      lastItemId: cart.lastItemId,
    };

    cart = reducer(
      cart,
      actions.setItemDiscount(newDisc)
    );

    expect(cart).toEqual(expectedCart);

    expect(cart.selectedItem.disc)
      .toBeGreaterThanOrEqualTo(0)
      .toBeLessThanOrEqualTo(100);
  });
});
