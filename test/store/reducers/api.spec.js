import expect from 'expect';
import reducer from '../../../app/store/reducers/api';
import * as actions from '../../../app/store/actions';
import { InitialState } from '../../../app/store/initial.state';

describe('reducer api', () => {
  const initState = InitialState.api;
  const initCustomerState = initState.customer;

  it('should handle initial state', () => {
    const state = reducer(
      undefined,
      {}
    );
    expect(state).toEqual(initState);
  });

  it('Customer : should handle ADD_CUSTOMERS', () => {
    const newCust = { name: 'xyz', email: 'xyz@gmail.com' };
    const newCust2 = { name: 'abc', email: 'abc@gmail.com' };
    const expectedState = Object.assign({}, initState, {
      customer: Object.assign({}, initCustomerState, {
        items: [newCust, newCust2],
      }),
    });

    const state = reducer(
      initState,
      actions.addCustomer(newCust)
    );

    const state2 = reducer(
      state,
      actions.addCustomer(newCust2)
    );
    newCust.id = 1;
    newCust2.id = 2;

    expect(state2).toEqual(expectedState);
  });

  it('Customer : should handle UPDATE_CUSTOMERS', () => {
    const newCust = { name: 'xyz', email: 'xyz@gmail.com' };
    const expectedState = Object.assign({}, initState, {
      customer: Object.assign({}, initCustomerState, {
        items: [newCust],
      }),
    });

    const state = reducer(
      initState,
      actions.addCustomer(newCust)
    );

    newCust.name = 'xyz123';

    const state2 = reducer(
      state,
      actions.updateCustomer(1, newCust)
    );
    expect(state2).toEqual(expectedState);
  });

  it('Customer : should handle REMOVE_CUSTOMERS', () => {
    const newCust = { name: 'xyz', email: 'xyz@gmail.com' };
    const expectedState = Object.assign({}, initState, {
      customer: Object.assign({}, initCustomerState),
    });
    const state = reducer(
      initState,
      actions.addCustomer(newCust)
    );

    const state2 = reducer(
      state,
      actions.removeCustomer(1)
    );
    expect(state2).toEqual(expectedState);
  });
});
