import expect from 'expect';
import reducer, { getNewTab } from '../../../app/store/reducers/cart';
import * as actions from '../../../app/store/actions';
import { InitialState } from '../../../app/store/initial.state';


describe('reducer cart', () => {

  const initialCarts = Object.assign({}, InitialState.cart);

  it('Cart : should return on initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      initialCarts
    );
  });

  it('Cart : should handle ADD_TAB', () => {
    const newTab = getNewTab(initialCarts.lastTabIndex);

    const expectedCarts = Object.assign({}, initialCarts, {
      tabs: [...initialCarts.tabs, newTab],
      selectedTab: 1,
      lastTabIndex: 2,
      calc: {
        feature: 'qty',
        impact: '+',
        init: false,
      },
    });

    const cart = reducer(
      initialCarts,
      actions.addTab()
    );
    expectedCarts.tabs[expectedCarts.selectedTab]
      .created_at = cart.tabs[cart.selectedTab]
      .created_at = new Date();
    expect(cart).toEqual(expectedCarts);
  });

  it('Cart : should handle SELECT_TAB', () => {

    const newTab1 = getNewTab(1);
    const newTab2 = getNewTab(2);

    const newCarts = {
      tabs: [newTab1, newTab2],
      selectedTab: 1,
      lastTabIndex: 2,
      calc: {
        feature: 'qty',
        impact: '+',
        init: false,
      },
    };

    const expectedCarts = Object.assign({}, newCarts, {
      selectedTab: 0,
    });

    const cart = reducer(
      newCarts,
      actions.selectTab(0)
    );

    expect(cart).toEqual(expectedCarts);
  });

  it('Cart : should handle REMOVE_TAB', () => {

    const newTab1 = getNewTab(1);
    const newTab2 = getNewTab(2);

    const newCarts = {
      tabs: [newTab1, newTab2],
      selectedTab: 1,
      lastTabIndex: 2,
      calc: {
        feature: 'qty',
        impact: '+',
        init: false,
      },
    };

    const expectedCarts = Object.assign({}, newCarts, {
      tabs: [newTab1],
      selectedTab: 0,
    });

    const cart = reducer(
      newCarts,
      actions.removeTab()
    );

    expect(cart).toEqual(expectedCarts);
  });

  it('Cart : should handle SELECT_CUSTOMER', () => {
    const newTab1 = getNewTab(1);
    const newCarts = {
      tabs: [Object.assign({}, newTab1)],
      selectedTab: 0,
    };
    newTab1.selectedCustomer = 5;

    const expectedCarts = Object.assign({}, newCarts, {
      tabs: [newTab1],
    });

    const cart = reducer(
      newCarts,
      actions.selectCustomer(5)
    );

    expect(cart).toEqual(expectedCarts);
  });

  it('Cart : should handle DESELECT_CUSTOMER', () => {
    const newTab1 = getNewTab(1);
    const newCarts = {
      tabs: [Object.assign({}, newTab1)],
      selectedTab: 0,
    };
    newTab1.selectedCustomer = 0;

    const expectedCarts = Object.assign({}, newCarts, {
      tabs: [newTab1],
    });

    const cart = reducer(
      newCarts,
      actions.deSelectCustomer()
    );

    expect(cart).toEqual(expectedCarts);
  });
});
