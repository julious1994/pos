import expect from 'expect';
import reducer, { FEATURE } from '../../../app/store/reducers/calc';
import cartReducer, { generateNewCartItem } from '../../../app/store/reducers/cart-items';
import * as actions from '../../../app/store/actions';
import { InitialState } from '../../../app/store/initial.state';
import { products } from '../../data';

describe('reducer calc', () => {
  const [newItem] = products;

  let carts = InitialState.cart;
  let selectedCart = carts.tabs[carts.selectedTab];

  const newOutcomeItem = generateNewCartItem({ item: newItem }, 0);

  const expectedItem = Object.assign({}, newOutcomeItem);

  selectedCart = cartReducer(
    selectedCart,
    actions.addItem(newItem)
  );

  carts.tabs[carts.selectedTab] = selectedCart;
  const selectedCartItem = carts.tabs[carts.selectedTab].selectedItem;

  it('Cart Calc : should handle press sequence [7,8,6,.,0,8,5], qty : 786.085', () => {
    carts = reducer(
      carts,
      actions.pressCalc('7')
    );
    carts = reducer(
      carts,
      actions.pressCalc('8')
    );
    carts = reducer(
      carts,
      actions.pressCalc('6')
    );
    carts = reducer(
      carts,
      actions.pressCalc('.')
    );
    carts = reducer(
      carts,
      actions.pressCalc('0')
    );
    carts = reducer(
      carts,
      actions.pressCalc('8')
    );
    carts = reducer(
      carts,
      actions.pressCalc('5')
    );

    expectedItem.qty = '786.085';
    expect(selectedCartItem)
      .toEqual(expectedItem);
  });


  it('Cart Calc : should handle press sequence [del, del], qty : 786.0', () => {
    carts = reducer(
      carts,
      actions.pressCalc(FEATURE.DEL)
    );
    carts = reducer(
      carts,
      actions.pressCalc(FEATURE.DEL)
    );
    expectedItem.qty = '786.0';
    expect(selectedCartItem)
      .toEqual(expectedItem);
  });


  it('Cart Calc : should handle press (mode price) [rate], mode : rate', () => {
    carts = reducer(
      carts,
      actions.pressCalc(FEATURE.RATE)
    );
    expect(carts.calc).
      toEqual({
        init: false,
        feature: FEATURE.RATE,
        impact: '+',
      });
  });

  it('Cart Calc : should handle press sequence [5, 0], rate : 50', () => {
    carts = reducer(
      carts,
      actions.pressCalc('5')
    );
    carts = reducer(
      carts,
      actions.pressCalc('0')
    );
    expectedItem.rate = '50';
    expect(selectedCartItem)
      .toEqual(expectedItem);
  });

  it('Cart Calc : should handle press sequence [DISC, 1, 0], Disc : 10', () => {
    carts = reducer(
      carts,
      actions.pressCalc(FEATURE.DISC)
    );
    carts = reducer(
      carts,
      actions.pressCalc('1')
    );
    carts = reducer(
      carts,
      actions.pressCalc('0')
    );
    expectedItem.disc = '10';
    expect(selectedCartItem)
      .toEqual(expectedItem);

    expect(Number(selectedCartItem.disc))
      .toBeGreaterThanOrEqualTo(0)
      .toBeLessThanOrEqualTo(100);
  });
});
