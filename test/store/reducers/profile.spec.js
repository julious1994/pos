import expect from 'expect';
import reducer from '../../../app/store/reducers/profile';
import * as actions from '../../../app/store/actions';
import { InitialState } from '../../../app/store/initial.state';

describe('reducer profile', () => {
  const initState = InitialState.profile;

  it('should handle LOGIN_ATTEMPT', () => {
    const expectedState = Object.assign({}, initState, {
      isLoading: true,
    });

    const state = reducer(
      initState,
      actions.loginAttempt()
    );

    expect(state).toEqual(expectedState);
  });

  it('should handle LOGGED_IN', () => {
    const user = { name: 'mayank', age: 21 };

    const expectedState = {
      isLoading: false,
      didFailed: false,
      login: true,
      user,
    };

    const state = reducer(
      initState,
      actions.loggedIn(Object.assign({}, user))
    );

    expect(state).toEqual(expectedState);
  });

  it('should handle LOGGED_OUT', () => {
    const expectedState = {
      isLoading: false,
      didFailed: false,
      login: false,
      user: {},
    };

    const state = reducer(
      initState,
      actions.loggedOut()
    );

    expect(state).toEqual(expectedState);
  });

  it('should handle LOGIN_FAILED', () => {
    const expectedState = {
      isLoading: false,
      didFailed: true,
      login: false,
      user: {},
    };

    const state = reducer(
      initState,
      actions.loginFailed()
    );

    expect(state).toEqual(expectedState);
  });
});
