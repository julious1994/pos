import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import expect from 'expect';
import * as actions from '../../app/store/actions';
import * as types from '../../app/store/action.types';
import { InitialState } from '../../app/store/initial.state';
import { products } from '../data';

const middleware = [thunk];
const mockStore = configureMockStore(middleware);

/*
describe('async action', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('creates RECEIVE_PRODUCTS when fetching products has been done ', () => {
    nock('http://localhost:8080')
      .get('/data/products.json')
      .reply(200, products);

    const expectedActions = [
      { type: types.FETCH_PRODUCTS },
      { type: types.RECEIVE_PRODUCTS, products },
    ];
    const store = mockStore(InitialState);

    return store.dispatch(actions.fetchProducts())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });
});
*/
