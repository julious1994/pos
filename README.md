Axelor Point of Sale
================================

Axelor POS include features like  Login, Add product to Cart, Manage Customers, Payments.

Installation
-----------------------------------------

#### Clone the latest project with SSH.
```bash
$ git clone git@git.axelor.com:ng/axelor-pos.git
```
#### Build  the Project
```bash
$ cd /path/to/axelor-pos
$ git checkout prototype2
$ npm install
```

#### Run the Project ( login credentials : type same username,password [like admin,admin] )
```bash
$ npm run dev
```

#### Test the Project ( unit test )
```bash
$ npm test
```