import { connect } from 'react-redux';
import * as ACTION from '../store/actions';
import Payment from '../components/Payment';

const mapStateToProps = ({ cart, api }) => {
  const { selectedTab, tabs } = cart;
  const { customer } = api;
  return {
    cart: tabs[selectedTab],
    customers: customer.items,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onAdd(tender, method) {
    dispatch(ACTION.addTender(tender, method));
  },
  onUpdate(index, tender) {
    dispatch(ACTION.updateTender(index, tender));
  },
  onRemove(index) {
    dispatch(ACTION.removeTender(index));
  },
  onSelect(index) {
    dispatch(ACTION.selectTender(index));
  },
  onCalcPress(key) {
    dispatch(ACTION.pressTenderCalc(key));
  },
  onInvoice() {
    dispatch(ACTION.setInvoice());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Payment);
