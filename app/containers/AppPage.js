import React, { Component } from 'react';
import store from '../store/index';
import * as ACTION from '../store/actions';
import { onLogin } from '../routes';

const initializeStore = () => {
  store.dispatch(ACTION.fetchCustomers());
  store.dispatch(ACTION.fetchProducts());
  store.dispatch(ACTION.fetchCategories());
};

export default class AppPage extends Component {
  componentWillMount() {
    const { profile } = store.getState();

    if (profile.login === false) {
      onLogin();
    } else {
      initializeStore();
    }
  }

  render() {
    return (
      <div className="app-wrapper">
        {this.props.children}
      </div>
    );
  }
}

AppPage.propTypes = {
  children: React.PropTypes.any,
};
