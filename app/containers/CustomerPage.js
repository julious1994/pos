import { connect } from 'react-redux';
import * as ACTION from '../store/actions';
import Customer from '../components/Customer';

const mapStateToProps = ({ filters, api, cart }) => {
  const { selectedTab, tabs } = cart;
  const { customer } = api;
  return {
    customer,
    filters: filters.customer,
    selectedCustomer: tabs[selectedTab].selectedCustomer,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onSelection(customer) {
    dispatch(ACTION.selectCustomer(customer));
  },
  onSelectionRemove() {
    dispatch(ACTION.deSelectCustomer());
  },
  onAdd(data) {
    dispatch(ACTION.addCustomer(data));
  },
  onUpdate(id, data) {
    dispatch(ACTION.updateCustomer(id, data));
  },
  onDelete(id) {
    dispatch(ACTION.removeCustomer(id));
  },
  onSearch(keyword) {
    dispatch(ACTION.searchCustomer(keyword));
  },
  onSort(sortField) {
    dispatch(ACTION.sortCustomer(sortField));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Customer);
