import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as ACTION from '../store/actions';
import Cart from '../components/Cart';
import Calc from '../components/Calc';
import ProductBar from '../components/ProductBar';
import TabBar from '../components/TabBar';
import ProductList from '../components/ProductList';

const styles = {
  main: {
    display: 'flex',
  },
  cart: {
    minWidth: 460,
    maxWidth: 460,
    padding: 10,
  },
  product: {
    padding: 10,
    flexGrow: 1,
  },
};

export const HomePage = (props) => {
  const { product, customer, categories, filters, cart, actions, api } = props;
  const { tabs, selectedTab, calc } = cart;
  const { selectedCustomer } = tabs[selectedTab];
  return (
    <div style={styles.main}>
      <div style={styles.cart}>
        <div>
          <Cart
            cart={tabs[selectedTab]}
            products={product}
            onItemSelect={actions.selectItem}
            loading={api.product.isLoading}
            />

          <Calc
            calc={calc}
            customers={customer}
            selectedCustomer={selectedCustomer}
            pressKey={actions.pressCalc}
            />
        </div>
      </div>
      <div style={styles.product}>
        <ProductBar title="Products"
          categories={categories}
          filters={filters}
          onCategoryFilter={actions.filterProductByCategory}
          onSort={actions.sortProducts}
          onSearch={actions.searchProducts}
          />
        <ProductList
          product={product}
          filters={filters}
          onItemClick={actions.addItem}
          loading={api.product.isLoading}
          />
        <TabBar
          tabs={tabs}
          selectedTab={selectedTab}
          onSelect={actions.selectTab}
          onAddTab={actions.addTab}
          onRemoveTab={actions.removeTab}
          />
      </div>
    </div>
  );
};

HomePage.propTypes = {
  api: PropTypes.any,
  filters: PropTypes.any,
  cart: PropTypes.any,
  categories: PropTypes.array,
  customer: PropTypes.array,
  product: PropTypes.array,
  actions: PropTypes.objectOf(PropTypes.func),
};

const mapStateToProps = ({ cart, filters, api }) => ({
  api,
  categories: api.category.items,
  product: api.product.items,
  customer: api.customer.items,
  filters: filters.product,
  cart,
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(ACTION, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
