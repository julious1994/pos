import React, { Component } from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import store from '../store/index';
import * as ACTION from '../store/actions';

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

export default class App extends Component {
  componentWillMount() {
    store.dispatch(ACTION.appInit());
  }

  render() {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme()}>
        <div>
          <Header />
            {this.props.children}
          <Footer />
        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  children: React.PropTypes.any,
};
