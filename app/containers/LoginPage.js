import { connect } from 'react-redux';
import Login from '../components/Login';

const mapStateToProps = ({ profile }) => ({ profile });

export default connect(mapStateToProps)(Login);
