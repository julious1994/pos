import { connect } from 'react-redux';
import Receipt from '../components/Receipt';

const mapStateToProps = ({ cart }) => {
  const { selectedTab, tabs } = cart;
  return {
    cart: tabs[selectedTab],
  };
};

export default connect(mapStateToProps, {})(Receipt);
