import request from 'request';
import Rx from 'rxjs/Rx';
import store from '../store/index';

export class AxelorService {
  constructor(baseURL) {
    this.baseURL = baseURL;
    this.restURL = `${baseURL}ws/rest/`;

    this.plainRequest = request.defaults({
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
      },
      json: true,
    });

    this.request = this.plainRequest.defaults({
      withCredentials: true,
    });
  }

  login(_username, _password) {
    const { profile } = store.getState();
    const username = _username || profile.user.username;
    const password = _password || profile.user.password;

    return Rx.Observable.create(observer => {
      const options = {
        url: `${this.baseURL}login.jsp`,
        body: { username, password },
      };
      this.plainRequest.post(options, (err, res, json) => {
        if (!err && res.statusCode === 200) {
          observer.next(json);
          observer.complete();
        } else {
          observer.error();
        }
      });
    });
  }

  logout() {
    return Rx.Observable.create(observer => {
      request.get(`${this.baseURL}logout`, (err, res, json) => {
        if (!err && res.statusCode === 200) {
          observer.next(json);
          observer.complete();
        } else {
          observer.error();
        }
      });
    });
  }

  interceptor(method, options) {
    return Rx.Observable.create(observer => {
      const params = options;
      params.method = method;
      this.request(params, (err, res, json) => {
        if (!err && res.statusCode === 200) {
          observer.next(json);
          observer.complete();
        } else {
          observer.error(err);
        }
      });
    }).retryWhen((errors) => {
      this.login().subscribe();
      return errors.delay(2000);
    }).timeout(20000);
  }

  get(url) {
    return this.interceptor('GET', { url });
  }

  post(url, body) {
    return this.interceptor('POST', { url, body });
  }

  put(url, body) {
    return this.interceptor('PUT', { url, body });
  }

  delete(url) {
    return this.interceptor('DELETE', { url });
  }
}

export class AxelorRestService extends AxelorService {
  search(entity, data = {}, fields = [], sortBy = null, limit = 40, offset = 0) {
    return this.post(`${this.restURL}${entity}/search`,
      {
        fields,
        sortBy,
        data,
        limit,
        offset,
      }
    );
  }

  fetch(entity, id, data = {}) {
    return this.post(`${this.restURL}${entity}/${id}/fetch`, { data });
  }

  read(entity, id) {
    return this.get(`${this.restURL}${entity}/${id}`);
  }

  create(entity, data) {
    return this.put(`${this.restURL}${entity}`, { data });
  }

  update(entity, data) {
    return this.post(`${this.restURL}${entity}`, { data });
  }

  delete(entity, id) {
    return super.delete(`${this.restURL}${entity}/${id}`);
  }
}

export default AxelorRestService;
