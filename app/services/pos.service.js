import AxelorRest from './rest.service';
import { Observable } from 'rxjs/Observable';

export const absServer = 'http://localhost:8080/abs-webapp/';
const REST = new AxelorRest(absServer);

export function login(username, password) {
  return REST.login(username, password);
}

export function logout() {
  return REST.logout();
}

export function getProducts(options = {}, limit = 10) {
  const fields = [
    'id',
    'fullName',
    'picture',
    'saleCurrency.symbol',
    'salePrice',
    'productCategory',
  ];

  const domain = {
    _domain: `self.isModel = :_isModel AND
      self.productTypeSelect = :_productTypeSelect AND self.expense = false`,
    _domainContext:
    {
      _isModel: false,
      _productTypeSelect: 'storable',
    },
  };

  const data = Object.assign(domain, options);

  return Observable.create(observer => {
    REST.search('com.axelor.apps.base.db.Product', data, fields, null, limit)
      .subscribe(
      (body) => {
        const json = [];
        body.data.forEach((item) => {
          const product = { id: item.id, name: item.fullName };
          product.category = item.productCategory.id;
          product.thumb = `${REST.restURL}com.axelor.meta.db.MetaFile/${item.picture.id}/content/download?image=true&v=2`;
          product.rate = Number(item.salePrice).toFixed(2);
          json.push(product);
        });
        observer.next(json);
        observer.complete();
      },
      (err) => {
        observer.error(err);
      }
    );
  });
}

export function getCategory() {
  const fields = [
    'id',
    'name',
    'code',
  ];

  return Observable.create(observer => {
    REST.search('com.axelor.apps.base.db.ProductCategory', {}, fields)
      .subscribe(
      (body) => {
        observer.next(body.data);
        observer.complete();
      },
      (err) => {
        observer.error(err);
      }
    );
  });
}

export function getCustomers() {
  const fields = [
    'id',
    'address',
    'emailAddress.address',
    'fullName',
    'fixedPhone',
    'picture',
  ];
  const data = {
    _domain: 'self.isContact != true',
    _domainContext: {
      'json-enhance': true,
      searchFields: 'name;firstName',
    },
  };

  return Observable.create(observer => {
    REST.search('com.axelor.apps.base.db.Partner', data, fields)
      .subscribe(
      (body) => {
        const json = [];
        body.data.forEach((item) => {
          const customer = { id: item.id || 0, name: item.fullName };
          customer.thumb = `${REST.restURL}com.axelor.meta.db.MetaFile/${item.picture.id}/content/download?image=true&v=2`;
          customer.email = item['emailAddress.address'];
          customer.address = item.address.fullName;
          customer.phone = item.fixedPhone;
          json.push(customer);
        });
        observer.next(json);
        observer.complete();
      },
      (err) => {
        observer.error(err);
      }
    );
  });
}
