export const category = [
  { "id": 1, "name": "Cold Drinks" },
  { "id": 2, "name": "Vegetables" },
  { "id": 3, "name": "Fruits" },
  { "id": 4, "name": "Services" },
  { "id": 5, "name": "Food" },
  { "id": 6, "name": "Others" }
];

export const customers = [
  { "id": 1, "name": "Mathew Hayden", "address": "69 rue de Namur, 1300 Wavre, Belgium", "phone": "+32 10 588 558"},
  { "id": 2, "name": "Grame Swan", "address": "#8, 1 st Floor,iscore complex, 607308 New Delhi TN, India", "phone": "+42 89 123 789"},
  { "id": 3, "name": "Chris Gayle", "address": "3661 Station Street, 94538 Fremont, United States", "phone": "+65 78 582 456"},
  { "id": 4, "name": "Virat Kohli", "address": "69 rue de Namur, 1300 Wavre, Belgium", "phone": "+75 56 258 369"},
  { "id": 5, "name": "AB Devillers", "address": "3661 Station Street, 94538 Fremont, United States", "phone": "+34 40 123 657"},
  { "id": 6, "name": "Shane Watson", "address": "#8, 1 st Floor,iscore complex, 607308 New Delhi TN, India", "phone": "+89 85 745 698"},
  { "id": 7, "name": "Mandeep Singh", "address": "#8, 1 st Floor,iscore complex, 607308 New Delhi TN, India", "phone": "+42 89 123 789"},
  { "id": 8, "name": "Chris Jordan", "address": "3661 Station Street, 94538 Fremont, United States", "phone": "+65 78 582 456"},
  { "id": 9, "name": "Travis Head", "address": "69 rue de Namur, 1300 Wavre, Belgium", "phone": "+75 56 258 369"},
  { "id": 10, "name": "Brendom Mcullam", "address": "3661 Station Street, 94538 Fremont, United States", "phone": "+34 40 123 657"},
  { "id": 11, "name": "Steven Smith", "address": "#8, 1 st Floor,iscore complex, 607308 New Delhi TN, India", "phone": "+89 85 745 698"},
  { "id": 12, "name": "Micheal Hussey", "address": "#8, 1 st Floor,iscore complex, 607308 New Delhi TN, India", "phone": "+42 89 123 789"},
  { "id": 13, "name": "Andre Tye", "address": "69 rue de Namur, 1300 Wavre, Belgium", "phone": "+32 10 588 558"},
  { "id": 14, "name": "Dale Steyn", "address": "#8, 1 st Floor,iscore complex, 607308 New Delhi TN, India", "phone": "+42 89 123 789"},
  { "id": 15, "name": "Suresh Raina", "address": "3661 Station Street, 94538 Fremont, United States", "phone": "+65 78 582 456"}
]

export const products = [
  { "id": 1, "name": "Carrot", "rate": "2.00", "interval": 1, "measure": "Kg", "thumb": "img/carrot.png", "category": 2 },
  { "id": 2, "name": "Cocacola", "rate": "0.51", "interval": 1, "measure": "Unit", "thumb": "img/cocacola-drink.png", "category": 1 },
  { "id": 3, "name": "Fanta Orange", "rate": "0.84", "interval": 1, "measure": "Unit", "thumb": "img/fanta.png", "category": 1 },
  { "id": 4, "name": "Lemon", "rate": "15.00", "interval": 1, "measure": "Kg", "thumb": "img/lemon.png", "category": 2 },
  { "id": 5, "name": "Tomato", "rate": "12.00", "interval": 1, "measure": "Kg", "thumb": "img/tomato.png", "category": 2 },
  { "id": 6, "name": "stawberry", "rate": "58.00", "interval": 1, "measure": "Kg", "thumb": "img/stawberry.png", "category": 3 },
  { "id": 7, "name": "Cup Cake", "rate": "3.00", "interval": 1, "measure": "Unit", "thumb": "img/cup-cake.png", "category": 5 },
  { "id": 8, "name": "Cherry", "rate": "15.00", "interval": 1, "measure": "Kg", "thumb": "img/cherry.png", "category": 3 },
  { "id": 10, "name": "Orange", "rate": "40.00", "interval": 1, "measure": "Kg", "thumb": "img/orange.png", "category": 3 },
  { "id": 11, "name": "Banana", "rate": "3.00", "interval": 12, "measure": "Unit", "thumb": "img/banana.png", "category": 3 },
  { "id": 12, "name": "Burger", "rate": "2.51", "interval": 1, "measure": "Unit", "thumb": "img/burger.png", "category": 5 },
  { "id": 13, "name": "Cake", "rate": "25.84", "interval": 1, "measure": "Kg", "thumb": "img/cake.png", "category": 5 },
  { "id": 14, "name": "Finger Chips", "rate": "5.00", "interval": 1, "measure": "Unit", "thumb": "img/finger-chips.jpg", "category": 5 },
  { "id": 15, "name": "Onion", "rate": "10.00", "interval": 1, "measure": "Kg", "thumb": "img/onion.png", "category": 2 },
  { "id": 16, "name": "Potato", "rate": "18.00", "interval": 1, "measure": "Kg", "thumb": "img/potato.png", "category": 2 },
  { "id": 17, "name": "Red Pepper", "rate": "5.00", "interval": 1, "measure": "Kg", "thumb": "img/red-pepper.png", "category": 2 },
  { "id": 18, "name": "Mango", "rate": "70.00", "interval": 1, "measure": "Kg", "thumb": "img/mango.png", "category": 3 },
  { "id": 19, "name": "Pastry", "rate": "40.00", "interval": 1, "measure": "Kg", "thumb": "img/pastry.png", "category": 5 },
  { "id": 20, "name": "Pepsi 100ml", "rate": "20.00", "interval": 1, "measure": "Unit", "thumb": "img/pepsi.png", "category": 1 },
  { "id": 21, "name": "Cherry2", "rate": "15.00", "interval": 1, "measure": "Kg", "thumb": "img/cherry.png", "category": 3 },
  { "id": 23, "name": "Egg Soluton", "rate": "999.00", "interval": 1, "measure": "Kg", "thumb": "img/eggs.png", "category": 4 },
  { "id": 29, "name": "Partner Service", "rate": "100.00", "interval": 1, "measure": "Kg", "thumb": "img/partner.png", "category": 4 },
  { "id": 56, "name": "stawberry2", "rate": "58.00", "interval": 1, "measure": "Kg", "thumb": "img/stawberry.png", "category": 3 },
  { "id": 84, "name": "Finger Chips2", "rate": "5.00", "interval": 1, "measure": "Unit", "thumb": "img/finger-chips.jpg", "category": 5 },
  { "id": 62, "name": "Cocacola 200ml", "rate": "0.51", "interval": 1, "measure": "Unit", "thumb": "img/cocacola-drink.png", "category": 1 }
]

