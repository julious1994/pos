import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import * as ACTION from './actions';
import * as DISPATCH_ACTION from './action.types';
import rootReducer from './reducers';
import { InitialState } from './initial.state';
import { hashHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import { persistState } from 'redux-devtools';

let store;

const router = routerMiddleware(hashHistory);

function logger({ getState }) {
  return (next) => (action) => {
    console.log('will dispatch', action);

    if (action.type === DISPATCH_ACTION.APP_INIT) {
      try {
        const localStore = JSON.parse(
          localStorage.getItem('axelor.pos.cart')
        );
        if (localStore) {
          store.dispatch(ACTION.appSet(localStore));
        }
      } catch (e) {
        console.log('Local Storage not supported');
      }
    }

    const returnValue = next(action);

    if (action.type !== DISPATCH_ACTION.APP_INIT && action.type !== '@@router/LOCATION_CHANGE') {
      const { cart, profile } = getState();
      localStorage.setItem('axelor.pos.cart', JSON.stringify({ cart, profile }));
    }

    console.log('state after dispatch', getState());

    return returnValue;
  };
}


const enhancer = compose(
  applyMiddleware(thunk, router, logger),
  persistState(
    window.location.href.match(
      /[?&]debug_session=([^&]+)\b/
    )
  )
);

// export function configureStore() {
//   return createStore(
//     rootReducer,
//     InitialState,
//     applyMiddleware(logger, thunk)
//   );
// }

export function configureStore() {
  return createStore(rootReducer, InitialState, enhancer);
}

export default store = configureStore();
