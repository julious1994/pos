import * as ACTION from './action.types';
import * as DATA from './data';
// import * as POS from '../services/pos.service';

export const dataURL = 'http://localhost:8008/data/';
export const DEVELOPMENT = true;

/* App */
export const appInit = () => ({
  type: ACTION.APP_INIT,
});

export const appSet = (payload) => ({
  type: ACTION.APP_SET,
  payload,
});

export const appReset = () => ({
  type: ACTION.APP_RESET,
});

/* Login */

export const loggedIn = (user) => ({
  type: ACTION.LOGGED_IN,
  user,
});

export const loggedOut = () => ({
  type: ACTION.LOGGED_OUT,
});

export const loginAttempt = () => ({
  type: ACTION.LOGIN_ATTEMPT,
});

export const loginFailed = () => ({
  type: ACTION.LOGIN_FAILED,
});

export const login = (credentials) => {
  return dispatch => {
    const { username, password } = credentials;

    dispatch(loginAttempt());

    if (DEVELOPMENT === true) {
      setTimeout(() => {
        if (credentials.username === credentials.password) {
          dispatch(loggedIn({ name: credentials.username, username, password }));
        } else {
          dispatch(loginFailed());
        }
      }, 4000);
    } else {
      // POS.login(username, password).subscribe(
      //   () => {
      //     dispatch(loggedIn({ name: username, username, password }));
      //   },
      //   () => {
      //     dispatch(loginFailed());
      //   }
      // );
    }
  };
};

export const logout = () => {
  return dispatch => {
    dispatch(loggedOut());
    if (DEVELOPMENT !== true) {
      // POS.logout().subscribe();
    }
  };
};

/* Cart */
export const selectTab = (tab) => ({
  type: ACTION.SELECT_TAB,
  tab,
});

export const addTab = () => ({
  type: ACTION.ADD_TAB,
});

export const removeTab = () => ({
  type: ACTION.REMOVE_TAB,
});

/* Cart Items */
export const addItem = (item) => ({
  type: ACTION.ADD_ITEM,
  item,
});

export const selectItem = (itemId) => ({
  type: ACTION.SELECT_ITEM,
  itemId,
});

export const removeItem = () => ({
  type: ACTION.REMOVE_ITEM,
});

export const nullItem = () => ({
  type: ACTION.NULL_ITEM,
});

export const setItemQty = (qty) => ({
  type: ACTION.SET_ITEM_QTY,
  qty,
});

export const setItemDiscount = (disc) => ({
  type: ACTION.SET_ITEM_DISC,
  disc,
});

export const setItemPrice = (price) => ({
  type: ACTION.SET_ITEM_PRICE,
  price,
});

/* API Product */
// async action

export const requestProducts = () => ({
  type: ACTION.FETCH_PRODUCTS,
});

export const receiveProducts = (data) => ({
  type: ACTION.RECEIVE_PRODUCTS,
  data,
});

export const failedProducts = () => ({
  type: ACTION.FAILED_PRODUCTS,
});

export const fetchProducts = (data = {}) => {
  return dispatch => {
    dispatch(requestProducts());

    if (DEVELOPMENT === true) {
      // fetch(`${dataURL}products.json`)
      //   .then(
      //   res => res.json(),
      //   () => dispatch(failedProducts())
      //   ).then(
      //   json => {
      setTimeout(() => { dispatch(receiveProducts(DATA.products)); }, 3000);
        // }
        // );
    } else {
      // POS.getProducts(data).subscribe(
      //   (json) => {
      //     dispatch(receiveProducts(json));
      //   },
      //   () => {
      //     dispatch(failedProducts());
      //   }
      // );
    }
  };
};

/* API Category */
// async action

export const requestCategories = () => ({
  type: ACTION.FETCH_CATEGORY,
});

export const receiveCategories = (data) => ({
  type: ACTION.RECEIVE_CATEGORY,
  data,
});

export const failedCategories = () => ({
  type: ACTION.FAILED_CATEGORY,
});

export const fetchCategories = () => {
  return dispatch => {
    dispatch(requestCategories());

    if (DEVELOPMENT === true) {
      // fetch(`${dataURL}category.json`)
      //   .then(
      //   res => res.json(),
      //   () => dispatch(failedCategories())
      //   ).then(
      //   json => dispatch(receiveCategories(json))
      //   );
      dispatch(receiveCategories(DATA.category));
    } else {
      // POS.getCategory()
      //   .subscribe(
      //   (json) => {
      //     dispatch(receiveCategories(json));
      //   },
      //   () => {
      //     dispatch(failedCategories());
      //   }
      //   );
    }
  };
};

/* Product Filters */
export const filterProductByCategory = (categoryId) => ({
  type: ACTION.FILTER_PRODUCT_CATEGORY,
  categoryId,
});

export const searchProducts = (keyword) => ({
  type: ACTION.SEARCH_PRODUCTS,
  keyword,
});

export const sortProducts = (sortField) => ({
  type: ACTION.SORT_PRODUCTS,
  sortField,
});

/* CALC */
export const pressCalc = (key) => ({
  type: ACTION.CALC_PRESS,
  key,
});

/* API Customer */
export const requestCustomers = () => ({
  type: ACTION.FETCH_CUSTOMERS,
});

export const receiveCustomers = (data) => ({
  type: ACTION.RECEIVE_CUSTOMERS,
  data,
});

export const failedCustomers = () => ({
  type: ACTION.FAILED_CUSTOMERS,
});

// async action
export const fetchCustomers = () => {
  return dispatch => {
    dispatch(requestCustomers());

    if (DEVELOPMENT === true) {
      // fetch(`${dataURL}customers.json`)
      //   .then(
      //   res => res.json(),
      //   () => dispatch(failedCustomers())
      //   ).then(
      //   json => dispatch(receiveCustomers(json))
      //   );
      dispatch(receiveCustomers(DATA.customers));
    } else {
      // POS.getCustomers()
      //   .subscribe(
      //   (json) => {
      //     dispatch(receiveCustomers(json));
      //   },
      //   () => {
      //     dispatch(failedCustomers());
      //   }
      //   );
    }
  };
};

export const selectCustomer = (customer) => ({
  type: ACTION.SELECT_CUSTOMER,
  customer,
});

export const deSelectCustomer = () => ({
  type: ACTION.DESELECT_CUSTOMER,
});

export const addCustomer = (customer) => ({
  type: ACTION.ADD_CUSTOMER,
  customer,
});

export const updateCustomer = (id, customer) => ({
  type: ACTION.UPDATE_CUSTOMER,
  id,
  customer,
});

export const removeCustomer = (id) => ({
  type: ACTION.REMOVE_CUSTOMER,
  id,
});

export const searchCustomer = (keyword) => ({
  type: ACTION.SEARCH_CUSTOMER,
  keyword,
});

export const sortCustomer = (sortField) => ({
  type: ACTION.SORT_CUSTOMER,
  sortField,
});

export const addTender = (tender, method) => ({
  type: ACTION.ADD_TENDER,
  tender,
  method,
});

export const updateTender = (index, tender) => ({
  type: ACTION.UPDATE_TENDER,
  index,
  tender,
});

export const removeTender = (index) => ({
  type: ACTION.REMOVE_TENDER,
  index,
});

export const selectTender = (index) => ({
  type: ACTION.SELECT_TENDER,
  index,
});

export const setInvoice = () => ({
  type: ACTION.SET_INVOICE,
});

export const pressTenderCalc = (key) => ({
  type: ACTION.TENDER_CALC_PRESS,
  key,
});
