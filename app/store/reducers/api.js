import { InitialState } from './../initial.state';
import * as ACTION from './../action.types';
import { combineReducers } from 'redux';

function apiProducts(state = InitialState.api.product, action) {
  switch (action.type) {
    case ACTION.FETCH_PRODUCTS:
      return Object.assign({}, state, {
        isLoading: true,
      });
    case ACTION.RECEIVE_PRODUCTS:
      return Object.assign({}, state, {
        items: action.data,
        didFailed: false,
        isLoading: false,
      });
    case ACTION.FAILED_PRODUCTS:
      return Object.assign({}, state, {
        didFailed: true,
        isLoading: false,
      });
    default:
      return state;
  }
}


function apiCategory(state = InitialState.api.category, action) {
  switch (action.type) {
    case ACTION.FETCH_CATEGORY:
      return Object.assign({}, state, {
        isLoading: true,
      });
    case ACTION.RECEIVE_CATEGORY:
      return Object.assign({}, state, {
        items: action.data,
        didFailed: false,
        isLoading: false,
      });
    case ACTION.FAILED_CATEGORY:
      return Object.assign({}, state, {
        didFailed: true,
        isLoading: false,
      });
    default:
      return state;
  }
}


function apiCustomer(state = InitialState.api.customer, action) {
  switch (action.type) {
    case ACTION.FETCH_CUSTOMERS:
      return Object.assign({}, state, {
        isLoading: true,
      });
    case ACTION.RECEIVE_CUSTOMERS:
      return Object.assign({}, state, {
        items: action.data,
        didFailed: false,
        isLoading: false,
      });
    case ACTION.FAILED_CUSTOMERS:
      return Object.assign({}, state, {
        didFailed: true,
        isLoading: false,
      });
    case ACTION.ADD_CUSTOMER:
      {
        const { customer } = action;
        const { items } = state;

        customer.id = (items.length > 0 ?
          items.map(c => c.id)
            .reduce((c1, c2) => (
              c1 > c2 ? c1 : c2
            )) : 0) + 1;
        return Object.assign({}, state, {
          items: [...state.items, Object.assign({}, customer)],
        });
      }
    case ACTION.UPDATE_CUSTOMER:
      {
        const { customer } = action;
        const custs = state.items.filter(c => c.id !== action.id);
        return Object.assign({}, state, {
          items: [...custs, Object.assign({}, customer)],
        });
      }
    case ACTION.REMOVE_CUSTOMER:
      {
        const custs = state.items.filter(c => c.id !== action.id);
        return Object.assign({}, state, {
          items: [...custs],
        });
      }
    default:
      return state;
  }
}

export default combineReducers({
  product: apiProducts,
  category: apiCategory,
  customer: apiCustomer,
});
