import { InitialState } from './../initial.state';
import * as ACTION from './../action.types';
import * as DISPATCH from './../actions';
import manageTender from './tender';
import { computeNumber, stripValue } from '../../utils';

export const MODE = {
  DEL: 'DEL',
  C: 'C',
};

export default function PaymentTenderCalc(tenderState = InitialState.cart.tabs[0], action) {
  let state = tenderState;
  let { payment } = state;
  let { tenders, selectedTender, calc } = payment;
  let initial = calc.init;
  let init = true;

  if (tenders.length === 0) {
    state = manageTender(state, DISPATCH.addTender('0', 'CASH (USD)'));
    payment = state.payment;
    tenders = payment.tenders;
    calc = payment.calc;
    selectedTender = 0;
    initial = true;
  }

  switch (action.type) {
    case ACTION.TENDER_CALC_PRESS:
      {
        const tender = tenders[selectedTender].value;
        switch (action.key) {
          case '+10':
          case '+20':
          case '+50':
            {
              const incr = Number(action.key);
              const value = Number(tender) + incr;
              state = manageTender(state, DISPATCH.updateTender(selectedTender, value.toString()));
            }
            break;
          case MODE.C:
            state = manageTender(state, DISPATCH.updateTender(selectedTender, '0'));
            init = false;
            break;
          default:
            {
              const { key } = action;
              const q = initial === true ? tender : key;
              let newQ = '';
              if (key === MODE.DEL) {
                newQ = initial === true ? stripValue(tender) : '0';
              } else {
                newQ = computeNumber({ initial, key, q, impact: '+' });
              }
              state = manageTender(state, DISPATCH.updateTender(selectedTender, newQ));
            }
            break;
        }
      }
      break;
    default:
      return state;
  }
  return Object.assign({}, state, {
    payment: Object.assign({}, payment, {
      calc: Object.assign({}, calc, {
        init,
      }),
    }),
  });
}


