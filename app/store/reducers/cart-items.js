import { InitialState } from './../initial.state';
import * as ACTION from './../action.types';

export function generateNewCartItem(action, itemId = 0) {
  const { item } = action;
  const { id, name, thumb, interval, rate, measure } = item;
  return {
    itemId,
    product: id,
    title: name,
    thumb,
    qty: interval || 1,
    rate,
    interval: interval || 1,
    measure: measure || 'unit',
    disc: 0,
  };
}

export function findItemById(items, id) {
  const findItems = items.filter(i => i.itemId === id);
  return findItems.length > 0 ? findItems[0] : null;
}

/* Internal Usage */
export default function ManageCartItems(state = InitialState.cart.tabs[0], action) {
  let item;
  let { items, lastItemId } = state;
  const { selectedItem } = state;

  switch (action.type) {
    case ACTION.ADD_ITEM:
      {
        let flag = true;
        for (const obj of items) {
          if (obj.product === action.item.id) {
            item = obj;
            item.qty = parseFloat(item.qty) + item.interval;
            flag = false;
            break;
          }
        }

        if (flag) {
          item = generateNewCartItem(action, lastItemId++);
          items = [...state.items, item];
        }

        return Object.assign({}, state, {
          items,
          lastItemId,
          selectedItem: item,
        });
      }
    case ACTION.SELECT_ITEM:
      {
        const findItem = findItemById(items, action.itemId);
        return Object.assign({}, state, {
          selectedItem: findItem,
        });
      }
    case ACTION.SET_ITEM_QTY:
    case ACTION.SET_ITEM_PRICE:
    case ACTION.SET_ITEM_DISC:
      item = findItemById(items, selectedItem.itemId);

      if (action.type === ACTION.SET_ITEM_QTY) {
        item.qty = action.qty;
      } else if (action.type === ACTION.SET_ITEM_DISC) {
        if (Number(action.disc) >= 100) {
          item.disc = 100;
        } else if (Number(action.disc) >= 0) {
          item.disc = action.disc;
        } else {
          item.disc = 0;
        }
      } else if (action.type === ACTION.SET_ITEM_PRICE) {
        item.rate = action.price;
      }

      return Object.assign({}, state, {
        items: [...items],
        selectedItem: Object.assign({}, item),
      });
    case ACTION.REMOVE_ITEM:
      {
        const remains = items.filter(v => v.itemId !== selectedItem.itemId);
        const newSelectItem = remains.length > 0 ? remains[remains.length - 1] : {};

        return Object.assign({}, state, {
          items: remains,
          selectedItem: newSelectItem,
        });
      }
    default:
      return state;
  }
}


