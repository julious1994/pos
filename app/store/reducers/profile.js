import { InitialState } from './../initial.state';
import * as ACTION from './../action.types';

export default function profile(state = InitialState.profile, action) {
  switch (action.type) {
    case ACTION.APP_SET:
      return action.payload.profile;
    case ACTION.LOGGED_IN:
      return Object.assign({}, state, {
        isLoading: false,
        didFailed: false,
        login: true,
        user: action.user,
      });
    case ACTION.LOGGED_OUT:
      return Object.assign({}, state, {
        isLoading: false,
        didFailed: false,
        login: false,
        user: {},
      });
    case ACTION.LOGIN_FAILED:
      return Object.assign({}, state, {
        isLoading: false,
        didFailed: true,
        login: false,
        user: {},
      });
    case ACTION.LOGIN_ATTEMPT:
      return Object.assign({}, state, {
        isLoading: true,
        didFailed: false,
        login: false,
        user: {},
      });
    default:
      break;
  }
  return state;
}
