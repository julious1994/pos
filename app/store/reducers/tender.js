import { InitialState } from './../initial.state';
import * as ACTION from './../action.types';
import manageCalc from './tender-calc';

export default function PaymentTenders(state = InitialState.cart.tabs[0], action) {
  const { payment } = state;
  const { tenders, selectedTender } = payment;

  switch (action.type) {
    case ACTION.ADD_TENDER:
      {
        const newTender = { value: action.tender, method: action.method };
        return Object.assign({}, state, {
          payment: Object.assign({}, payment, {
            tenders: [...tenders, newTender],
            selectedTender: tenders.length,
            calc: Object.assign({}, payment.calc, {
              init: false,
            }),
          }),
        });
      }
    case ACTION.REMOVE_TENDER:
      {
        tenders.splice(action.index, 1);
        return Object.assign({}, state, {
          payment: Object.assign({}, payment, {
            tenders: [...tenders],
            calc: Object.assign({}, payment.calc, {
              init: false,
            }),
          }),
        });
      }
    case ACTION.UPDATE_TENDER:
      {
        tenders[selectedTender].value = action.tender;
        return Object.assign({}, state, {
          payment: Object.assign({}, payment, {
            tenders: [...tenders],
          }),
        });
      }
    case ACTION.SELECT_TENDER:
      return Object.assign({}, state, {
        payment: Object.assign({}, payment, {
          selectedTender: action.index,
          calc: Object.assign({}, payment.calc, {
            init: false,
          }),
        }),
      });
    case ACTION.SET_INVOICE:
      return Object.assign({}, state, {
        payment: Object.assign({}, payment, {
          invoice: !payment.invoice,
        }),
      });
    case ACTION.TENDER_CALC_PRESS:
      return manageCalc(state, action);
    default:
      return state;
  }
}


