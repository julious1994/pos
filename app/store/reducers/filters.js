import { InitialState } from './../initial.state';
import * as ACTION from './../action.types';
import { combineReducers } from 'redux';

function productFilter(state = InitialState.filters.product, action) {
  switch (action.type) {
    case ACTION.FILTER_PRODUCT_CATEGORY:
      return Object.assign({}, state, {
        category: action.categoryId,
      });
    case ACTION.SORT_PRODUCTS:
      return Object.assign({}, state, {
        sort: action.sortField,
      });
    case ACTION.SEARCH_PRODUCTS:
      return Object.assign({}, state, {
        search: action.keyword,
      });
    default:
      break;
  }
  return state;
}

function customerFilter(state = InitialState.filters.customer, action) {
  switch (action.type) {
    case ACTION.SEARCH_CUSTOMER:
      return Object.assign({}, state, {
        search: action.keyword,
      });
    case ACTION.SORT_CUSTOMER:
      return Object.assign({}, state, {
        sort: action.sortField,
        asc: state.sort === action.sortField ? !state.asc : true,
      });
    default:
      return state;
  }
}

export default combineReducers({
  product: productFilter,
  customer: customerFilter,
});
