import { combineReducers } from 'redux';

import { routerReducer as routing } from 'react-router-redux';
import filters from './filters';
import api from './api';
import cart from './cart';
import profile from './profile';

export const rootReducer = combineReducers({
  cart,
  filters,
  api,
  profile,
  routing,
});

export default rootReducer;
