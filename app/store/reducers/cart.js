import { InitialState } from './../initial.state';
import * as ACTION from './../action.types';
import manageCalc from './calc';
import manageCartItems from './cart-items';
import managePayment from './tender';

// var cartItemId = 1;
// var tabIndex = 2;

export function getNewTab(lastTabIndex) {
  return Object.assign({}, InitialState.cart.tabs[0], {
    lastItemId: 1,
    tabIndex: lastTabIndex + 1,
    items: [],
    selectedItem: {},
    created_at: new Date(),
    selectedCustomer: 0,
    payment: {
      tenders: [],
      selectedTender: undefined,
      invoice: false,
      calc: {
        init: false,
      },
    },
  });
}

export function getCartItemStats(item) {
  const qty = Number(item.qty).toFixed(3);
  const disc = Number(item.disc).toFixed(2);
  const rate = Number(item.rate).toFixed(2);
  const total = Number(rate * qty).toFixed(2);
  const discount = Number(total * disc / 100).toFixed(2);
  const net = total - discount;

  return { qty, disc, rate, total, discount, net };
}

export function getCartTotal(items) {
  let netAmount = 0;
  items.forEach((item) => {
    const { net } = getCartItemStats(item);
    netAmount += net;
  });
  return netAmount;
}

export default function Cart(state = InitialState.cart, action) {
  let { selectedTab } = state;
  const { tabs } = state;
  const lastTab = tabs[tabs.length - 1];

  switch (action.type) {
    case ACTION.APP_SET:
      return action.payload.cart;
    case ACTION.ADD_TAB:
      return Object.assign({}, InitialState.cart, {
        tabs: [...tabs, getNewTab(state.lastTabIndex)],
        lastTabIndex: state.lastTabIndex + 1,
        selectedTab: tabs.length,
        calc: Object.assign({}, InitialState.cart.calc),
      });
    case ACTION.REMOVE_TAB:
      {
        const removeTab = tabs[selectedTab];

        tabs.splice(state.selectedTab, 1);

        if (removeTab.tabIndex === lastTab.tabIndex) {
          selectedTab--;
        }

        return Object.assign({}, state, {
          tabs: [...tabs],
          selectedTab,
          calc: Object.assign({}, InitialState.cart.calc),
        });
      }
    case ACTION.SELECT_TAB:
      return Object.assign({}, state, {
        selectedTab: action.tab,
        calc: Object.assign({}, InitialState.cart.calc),
      });
    case ACTION.ADD_ITEM:
    case ACTION.SELECT_ITEM:
    case ACTION.REMOVE_ITEM:
    case ACTION.NULL_ITEM:
    case ACTION.SET_ITEM_QTY:
    case ACTION.SET_ITEM_DISC:
    case ACTION.SET_ITEM_PRICE:
      tabs[selectedTab] = manageCartItems(tabs[selectedTab], action);
      return Object.assign({}, state, {
        tabs: [...tabs],
        calc: Object.assign({}, InitialState.cart.calc),
      });
    case ACTION.SELECT_CUSTOMER:
    case ACTION.DESELECT_CUSTOMER:
      tabs[selectedTab].selectedCustomer = action.customer ? action.customer : 0;
      return Object.assign({}, state, {
        tabs: [...tabs],
      });
    case ACTION.CALC_PRESS:
      return manageCalc(state, action);
    case ACTION.ADD_TENDER:
    case ACTION.REMOVE_TENDER:
    case ACTION.UPDATE_TENDER:
    case ACTION.SELECT_TENDER:
    case ACTION.TENDER_CALC_PRESS:
    case ACTION.SET_INVOICE:
      tabs[selectedTab] = managePayment(tabs[selectedTab], action);
      return Object.assign({}, state, {
        tabs: [...tabs],
      });
    default:
      return state;
  }
}
