import { InitialState } from './../initial.state';
import * as ACTION from './../action.types';
import * as DISPATCH_ACTION from './../actions';
import cart from './cart';
import { computeNumber, stripValue } from '../../utils';

export const FEATURE = {
  QTY: 'qty',
  RATE: 'rate',
  DISC: 'disc',
  DEL: 'del',
};

/* Internal Usage */
export default function ManageCalc(cartState = InitialState.cart, action) {
  let state = cartState;
  let init = true;
  let feature = action.key;

  const { calc } = state;
  let { impact } = calc;
  const { init: initial, feature: mode } = calc;
  const cartTab = state.tabs[state.selectedTab];

  /* No Action Needed */

  switch (action.type) {
    case ACTION.CALC_PRESS:
      switch (action.key) {
        case FEATURE.QTY:
        case FEATURE.RATE:
        case FEATURE.DISC:
          init = false;
          break;
        case '+/-':
          impact = impact === '+' ? '-' : '+';
          feature = mode;
          init = initial;
          break;
        default:
          {
            if (cartTab.items.length === 0) {
              return state;
            }
            const { selectedItem } = cartTab;
            const { key } = action;
            const q = initial === true ? selectedItem[mode] : key;
            let newQ = computeNumber({ initial, key, q, impact });

            if (key === FEATURE.DEL) {
              newQ = initial === true ? stripValue(selectedItem[mode]) : '0';
            }

            switch (mode) {
              case FEATURE.QTY:
                if (newQ.length === 0) {
                  state = cart(state, DISPATCH_ACTION.removeItem());
                  init = false;
                } else {
                  state = cart(state, DISPATCH_ACTION.setItemQty(newQ));
                }
                break;
              case FEATURE.DISC:
                state = cart(state, DISPATCH_ACTION.setItemDiscount(newQ));
                break;
              case FEATURE.RATE:
                state = cart(state, DISPATCH_ACTION.setItemPrice(newQ));
                break;
              default:
                break;
            }
            feature = mode;
          }
          break;
      }
      return Object.assign({}, state, {
        calc: Object.assign({}, calc, {
          init,
          feature,
          impact,
        }),
      });
    default:
      return state;
  }
}
