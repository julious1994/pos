export const InitialState = {
  cart: {
    tabs: [
      {
        items: [],
        selectedItem: {},
        created_at: new Date(),
        lastItemId: 0,
        tabIndex: 1,
        selectedCustomer: 0,
        payment: {
          tenders: [],
          selectedTender: undefined,
          invoice: false,
          calc: {
            init: false,
          },
        },
      },
    ],
    lastTabIndex: 1,
    selectedTab: 0,
    calc: {
      init: false,
      feature: 'qty',
      impact: '+',
    },
  },
  filters: {
    product: {
      search: '',
      sort: 'name',
      category: 0,
    },
    customer: {
      search: '',
      sort: 'name',
      asc: true,
    },
  },
  api: {
    product: {
      items: [],
      isLoading: false,
      didFailed: false,
    },
    category: {
      items: [],
      isLoading: false,
      didFailed: false,
    },
    customer: {
      items: [],
      isLoading: false,
      didFailed: false,
    },
  },
  profile: {
    isLoading: false,
    didFailed: false,
    login: false,
    user: {
    },
  },
};
