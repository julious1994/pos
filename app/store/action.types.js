export const APP_INIT = 'APP_INIT';
export const APP_SET = 'APP_SET';
export const APP_RESET = 'APP_RESET';

/* Actions Related To Login */
export const LOGGED_IN = 'LOGGED_IN';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const LOGIN_ATTEMPT = 'LOGIN_ATTEMPT';
export const LOGGED_OUT = 'LOGGED_OUT';

/* Actions Related To Cart Tabs */
export const SELECT_TAB = 'SELECT_TAB';
export const ADD_TAB = 'ADD_TAB';
export const REMOVE_TAB = 'REMOVE_TAB';

/* Actions Related TO Shopping Cart */
export const ADD_ITEM = 'ADD_ITEM';
export const SELECT_ITEM = 'SELECT_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const SET_ITEM_QTY = 'SET_ITEM_QTY';
export const SET_ITEM_DISC = 'SET_ITEM_DISC';
export const SET_ITEM_PRICE = 'SET_ITEM_PRICE';
export const NULL_ITEM = 'NULL_ITEM';

/* Actions Related TO Products Comes via API */
export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';
export const RECEIVE_PRODUCTS = 'RECEIVE_PRODUCTS';
export const FAILED_PRODUCTS = 'FAILED_PRODUCTS';

/* Actions Related TO Category Comes via API */
export const FETCH_CATEGORY = 'FETCH_CATEGORY';
export const RECEIVE_CATEGORY = 'RECEIVE_CATEGORY';
export const FAILED_CATEGORY = 'FAILED_CATEGORY';


/* Actions Related TO Filteration of Product */
export const FILTER_PRODUCT_CATEGORY = 'FILTER_PRODUCT_CATEGORY';
export const SEARCH_PRODUCTS = 'SEARCH_PRODUCTS';
export const SORT_PRODUCTS = 'SORT_PRODUCTS';

export const CALC_PRESS = 'CALC_PRESS';


/* Actions Related TO Customer Comes via API */
export const FETCH_CUSTOMERS = 'FETCH_CUSTOMERS';
export const RECEIVE_CUSTOMERS = 'RECEIVE_CUSTOMERS';
export const FAILED_CUSTOMERS = 'FAILED_CUSTOMERS';

export const SELECT_CUSTOMER = 'SELECT_CUSTOMER';
export const DESELECT_CUSTOMER = 'DESELECT_CUSTOMER';
export const ADD_CUSTOMER = 'ADD_CUSTOMER';
export const UPDATE_CUSTOMER = 'UPDATE_CUSTOMER';
export const REMOVE_CUSTOMER = 'REMOVE_CUSTOMER';
export const SEARCH_CUSTOMER = 'SEARCH_CUSTOMER';
export const SORT_CUSTOMER = 'SORT_CUSTOMER';

export const ADD_TENDER = 'ADD_TENDER';
export const UPDATE_TENDER = 'UPDATE_TENDER';
export const REMOVE_TENDER = 'REMOVE_TENDER';
export const SELECT_TENDER = 'SELECT_TENDER';
export const TENDER_CALC_PRESS = 'TENDER_CALC_PRESS';

export const SET_INVOICE = 'SET_INVOICE';
