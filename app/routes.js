import React from 'react';
import { Route, IndexRedirect, IndexRoute, browserHistory } from 'react-router';

import store from './store/index';
import { push } from 'react-router-redux';
import App from './containers/App';
import AppPage from './containers/AppPage';
import Login from './containers/LoginPage';
import Home from './containers/HomePage';
import PaymentContainer from './containers/PaymentPage';
import CustomerContainer from './containers/CustomerPage';
import ReceiptContainer from './containers/ReceiptPage';

export const LOGIN = '/login';
export const HOME = '/app';
export const CUSTOMER = `${HOME}/customer`;
export const PAYMENT = `${HOME}/payment`;
export const RECEIPT = `${HOME}/receipt`;

export const goTo = (path) => {
  store.dispatch(push(path));
};

export const goToQuery = (m, q) => {
  goTo(`${m}${q ? `?${q}` : ''}`);
};

export const onLogin = () => {
  goTo(LOGIN);
};

export const onHome = (q) => {
  goToQuery(HOME, q);
};

export const onCustomer = (q) => {
  goToQuery(CUSTOMER, q);
};

export const onPayment = (q) => {
  goToQuery(PAYMENT, q);
};

export const onReceipt = (q) => {
  goToQuery(RECEIPT, q);
};

export default (
  <Route path="/" component={App}>
    <IndexRedirect to={"/app"} />
    <Route path="login" component={Login}/>
    <Route path="app" component={AppPage}>
      <IndexRoute component={Home} />
      <Route path="payment" component={PaymentContainer} />
      <Route path="customer" component={CustomerContainer} />
      <Route path="receipt" component={ReceiptContainer} />
    </Route>
  </Route>
);
