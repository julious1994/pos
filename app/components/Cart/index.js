import React, { Component } from 'react';
import EventListener from 'react-event-listener';
import moment from 'moment';
import ReactDOM from 'react-dom';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import ShoppingCart from 'material-ui/svg-icons/action/shopping-cart';
import { List } from 'material-ui/List';
import { Toolbar, ToolbarGroup, ToolbarTitle } from 'material-ui/Toolbar';

import CartItem from '../CartItem';
import { formatNumber } from '../../utils';
import { getCartTotal } from '../../store/reducers/cart';
import styles from './style';

export default class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 450,
    };
  }

  componentDidMount() {
    this.handleScroll();
    this.updateWidth();
  }

  componentDidUpdate() {
    this.handleScroll();
  }

  updateWidth() {
    let height = 450;
    const maxHeight = 830;
    const { innerHeight } = window;
    height = innerHeight < maxHeight ? (height - (maxHeight - innerHeight)) : height;
    if (height !== this.state.height) {
      this.setState({ height });
    }
  }

  handleScroll() {
    let panel;
    let node;
    const listHeight = 0;

    if (this.refs.selectedCartItem) {
      panel = this.refs.listPanel;
      node = ReactDOM.findDOMNode(this.refs.selectedCartItem);
    }

    if (panel && node &&
      (node.offsetTop + listHeight > panel.scrollTop + panel.offsetHeight ||
        node.offsetTop + listHeight < panel.scrollTop)) {
      panel.scrollTop = node.offsetTop + listHeight - panel.offsetTop;
    }
  }

  renderCartItems() {
    const products = {};

    this.props.products.forEach(item => {
      products[item.id] = item;
    });

    const { onItemSelect, cart, loading } = this.props;
    const { items, selectedItem, tabIndex } = cart;
    const grandTotal = getCartTotal(items);

    return (
      <div>
        <List>
          {items.map((item) =>
            <div
              key={`${tabIndex}_${item.itemId}_${loading === true ? '1' : '0'}`}
              >
              <CartItem
                {...item}
                product={products[item.product]}
                selected={item.itemId === selectedItem.itemId}
                ref={item.itemId === selectedItem.itemId ?
                  'selectedCartItem' : `cart_item_${item.itemId}`}
                onSelect={() => onItemSelect(item.itemId) }
                />
              <Divider inset={true} />
            </div>
          ) }
        </List>
        {items.length > 0
          ? <div style={styles.cartTotal}>
            { 'Total: $'}
            <span style={styles.cartTotalDisplay}>
              { formatNumber(grandTotal) }
            </span>
          </div>
          : <div style={styles.emptyCart}>
            <IconButton
              iconStyle={styles.largeIcon}
              style={styles.large}
              >
              <ShoppingCart/>
            </IconButton>
            <h1 style={styles.emptyCartCaption}>
              Your Cart is Empty
            </h1>
          </div>
        }
      </div>
    );
  }

  render() {
    const { height } = this.state;
    const timeAgo = moment(this.props.cart.created_at).fromNow();
    const cartStyle = Object.assign({}, styles.cart, { height });
    return (
      <EventListener target="window" onResize={() => this.updateWidth()}>
        <div>
          <Toolbar>
            <ToolbarGroup >
              <ToolbarTitle text="Shopping Cart" />
            </ToolbarGroup>
            <ToolbarGroup style={{ float: 'right' }}>
              <p>{timeAgo}</p>
            </ToolbarGroup>
          </Toolbar>
          <div style={cartStyle} ref="listPanel">
            {this.renderCartItems() }
          </div>
        </div>
      </EventListener>
    );
  }
}

Cart.propTypes = {
  cart: React.PropTypes.any,
  products: React.PropTypes.array,
  loading: React.PropTypes.bool,
  onItemSelect: React.PropTypes.func,
};

Cart.defaultProps = {
  products: [],
  loading: false,
  onItemSelect: (id) => {
    console.log('item selected', id);
  },
};
