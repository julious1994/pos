export const styles = {
  largeIcon: {
    width: 60,
    height: 60,
  },
  large: {
    width: 120,
    height: 120,
    padding: 30,
  },
  cart: {
    overflow: 'auto',
    border: '2px dashed #eee',
  },
  cartTotal: {
    textAlign: 'right',
    padding: '10px',
    color: '#555',
    fontSize: '25px',
  },
  cartTotalDisplay: {
    fontSize: '23px',
  },
  emptyCart: {
    display: 'flex',
    flexFlow: 'column nowrap',
    alignItems: 'center',
  },
  emptyCartCaption: {
    margin: 0,
  },
};

export default styles;
