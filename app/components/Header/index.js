import React, { Component } from 'react';
import { connect } from 'react-redux';
import { onLogin } from '../../routes';
import * as ACTION from '../../store/actions';
import store from '../../store/index';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

export class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
  }

  handleToggle() {
    this.setState({ open: !this.state.open });
  }

  handleClose() {
    this.setState({ open: false });
  }

  signOut() {
    store.dispatch(
      ACTION.logout()
    );
    onLogin();
  }

  render() {
    return (
      <div>
        <AppBar
          title="Point of Sale"
          showMenuIconButton={false}
          iconElementRight={
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <span style={{ color: '#ffe', fontWeight: 'bold' }}>
                {this.props.profile.user.name}
              </span>
              <IconMenu
                iconButtonElement={
                  <IconButton><MoreVertIcon /></IconButton>
                }
                targetOrigin={{ horizontal: 'right', vertical: 'top' }}
                anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
                >
                <MenuItem primaryText="About" />
                <MenuItem primaryText="Sign out" onTouchTap={() => this.signOut()} />
              </IconMenu>
            </div>
          }
          />
      </div>
    );
  }
}

Header.propTypes = {
  profile: React.PropTypes.any,
};

const mapPropToState = (state) => ({
  profile: state.profile,
});

export default connect(mapPropToState)(Header);
