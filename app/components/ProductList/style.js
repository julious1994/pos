export const styles = {
  container: {
    display: 'block',
    overflow: 'auto',
    overflowX: 'hidden',
  },
  loaderContainer: {
    position: 'relative',
    textAlign: 'center',
    marginTop: '20px',
  },
  refresh: {
    display: 'inline-block',
    position: 'relative',
  },
};

export default styles;
