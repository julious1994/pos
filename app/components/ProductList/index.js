import React, { Component } from 'react';
import EventListener from 'react-event-listener';
import { GridList } from 'material-ui/GridList';
import Snackbar from 'material-ui/Snackbar';
import CircularProgress from 'material-ui/CircularProgress';

import Product from '../Product';
import styles from './style';

const _ = require('lodash');

export const XXL = 6;
export const XL = 5;
export const LG = 4;
export const MD = 3;
export const SM = 2;
export const XS = 1;

export default class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      items: 8,
      height: 700,
    };
  }

  componentDidMount() {
    this.updateWidth();
  }

  filterProducts() {
    const { filters, product } = this.props;
    const { category, search, sort } = filters;
    let items = product;

    if (category) {
      items = items.filter(item => item.category === category);
    }
    if (search) {
      items = items.filter(item =>
        item.name.toLowerCase().indexOf(search.toLowerCase()) > -1);
    }
    if (sort) {
      items = _.sortBy(items, sort, (n) => Math.sin(n));
    }
    return items;
  }

  addProductToCart(item) {
    this.setState({ open: true });
    this.props.onItemClick(item);
  }

  closeNotify() {
    this.setState({ open: false });
  }

  updateWidth() {
    const { innerWidth, innerHeight } = window;
    const maxHeight = 930;
    let items = 8;
    let height = 700;

    if (innerWidth >= 1540) {
      items = 8;
    } else if (innerWidth >= 1320) {
      items = 6;
    } else if (innerWidth >= 1200) {
      items = 4;
    } else if (innerWidth >= 1000) {
      items = 3;
    } else if (innerWidth >= 800) {
      items = 2;
    } else if (innerWidth >= 320) {
      items = 1;
    }
    height = innerHeight < maxHeight ? (height - (maxHeight - innerHeight)) : height;
    if (height !== this.state.height) {
      this.setState({ height });
    }
    if (items !== this.state.items) {
      this.setState({ items });
    }
  }

  render() {
    const products = this.filterProducts();
    const { items, height } = this.state;
    const container = Object.assign({}, styles.container, { height });
    return (
      <EventListener target="window" onResize={() => this.updateWidth()}>
        <div style={container}>
          {this.props.loading === true &&
              <div style={styles.loaderContainer}>
                <CircularProgress size={2} />
              </div>
          }

          <GridList cols={items}>
            {products.map((product) =>
              <Product
                key={product.id}
                {...product}
                onSelect={(item) => this.addProductToCart(item)}/>
            )}
          </GridList>

          {products.length === 0 && this.props.loading === false &&
            <h3>No Result Found</h3>
          }

          <Snackbar
            open={this.state.open}
            message={<b>Product Added to Cart !!!</b>}
            autoHideDuration={700}
            onRequestClose={() => this.closeNotify()}
            />
        </div>
      </EventListener>
    );
  }
}

ProductList.propTypes = {
  product: React.PropTypes.array,
  filters: React.PropTypes.any,
  loading: React.PropTypes.bool,
  onItemClick: React.PropTypes.func,
};

ProductList.defaultProps = {
  product: [],
  filters: {
    category: '',
    sort: '',
    search: '',
  },
  loading: false,
  onItemClick: (v) => {
    console.log('item selected', v);
  },
};
