import React, { PropTypes } from 'react';
import { GridTile } from 'material-ui/GridList';

export const Product = (props) => {
  const { id, name, rate, thumb, onSelect } = props;
  return (
    <GridTile
        key={id}
        title={<b>{name}</b>}
        subtitle={<span>$ <b>{rate}</b></span>}
        onTouchTap={() => onSelect(props)}
        style={{ cursor: 'pointer' }}
        >
        <img src={thumb} />
    </GridTile>
  );
};

Product.propTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  thumb: PropTypes.string,
  rate: PropTypes.any,
  onSelect: PropTypes.func.isRequired,
};

export default Product;
