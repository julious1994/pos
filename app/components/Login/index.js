import React, { Component } from 'react';
import { onHome } from '../../routes';
import store from '../../store/index';
import * as ACTION from '../../store/actions';
import style from './style';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Paper from 'material-ui/Paper';
import LinearProgress from 'material-ui/LinearProgress';
import Snackbar from 'material-ui/Snackbar';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = this.getDefaultState();
  }

  getDefaultState() {
    return {
      open: false,
      username: {
        value: '',
        error: '',
      },
      password: {
        value: '',
        error: '',
      },
    };
  }

  checkValidity() {
    const { username, password } = this.state;
    if (username.value === '') {
      return false;
    }
    if (password.value === '') {
      return false;
    }
    return true;
  }

  handleInput(field, e) {
    const value = e.target.value.trim();
    let error = '';

    switch (field) {
      case 'username':
        if (value.length === 0) {
          error = 'Username is required';
        }
        this.setState({ username: { value, error } });
        break;
      case 'password':
        if (value.length === 0) {
          error = 'Password is required';
        }
        this.setState({ password: { value, error } });
        break;
      default:
        break;
    }
  }

  componentDidMount() {
    this.authenticate(this.props, false);
  }

  componentWillReceiveProps(nextProp) {
    this.authenticate(nextProp);
  }

  authenticate(data, open = true) {
    const { profile } = data;
    if (profile.isLoading === false) {
      if (profile.login === true) {
        onHome();
      } else {
        this.setState({ open });
      }
    }
  }

  doLogin() {
    if (this.checkValidity() === true) {
      const { username, password } = this.state;
      store.dispatch(
        ACTION.login({
          username: username.value,
          password: password.value,
        })
      );
    }
  }

  doCancel() {
    this.setState(this.getDefaultState());
  }

  doForget() {
    // forget password logic
  }

  doCloseNotify() {
    this.setState({ open: false });
  }

  render() {
    const { username, password } = this.state;
    const { profile } = this.props;

    return (
        <div style={style.container}>
          <div style={style.page}>
            <form onSubmit={() => this.doLogin()}>
              <Paper zDepth={3} style={style.content}>
                  <h1> Sign in </h1>
                  {profile.isLoading === true &&
                    <LinearProgress mode="indeterminate" />
                  }
                  <Snackbar
                    open={this.state.open}
                    message="Invalid Credentials"
                    autoHideDuration={4000}
                    onRequestClose={() => this.doCloseNotify()}
                  />

                  <TextField
                    hintText="username"
                    floatingLabelText="Username"
                    value={username.value}
                    errorText={username.error}
                    onChange={(e) => this.handleInput('username', e)}
                    style={style.text}
                  />
                  <TextField
                    hintText="password"
                    floatingLabelText="Password"
                    value={password.value}
                    errorText={password.error}
                    onChange={(e) => this.handleInput('password', e)}
                    type="password"
                    style={style.text}
                  />
                  <div>
                    <RaisedButton label="Login"
                      primary={true}
                      style={style.btn}
                      disabled={ ! this.checkValidity() }
                      onTouchTap={() => this.doLogin()}
                      />
                    <RaisedButton label="Cancel"
                      secondary={true}
                      style={style.btn}
                      onTouchTap={() => this.doCancel()}
                      />
                  </div>
                  <div style={style.extra}>
                    <FlatButton label="forget password ?"
                      secondary={true}
                      onTouchTap={() => this.doForget()}
                      />
                  </div>
              </Paper>
            </form>
          </div>
        </div>
    );
  }
}

Login.propTypes = {
  profile: React.PropTypes.object,
};
