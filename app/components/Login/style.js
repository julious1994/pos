export const styles = {
  container: {
    height: '100%',
    width: '100%',
    background: '#eee',
    position: 'fixed',
  },
  page: {
    width: '400px',
    margin: '8% auto',
    height: '100%',
  },
  content: {
    padding: '10px 20px',
    display: 'flex',
    flexFlow: 'column nowrap',
  },
  text: {
    width: '100%',
  },
  btn: {
    margin: '30px 10px 10px 0',
  },
  extra: {
    alignSelf: 'flex-end',
  },
};

export default styles;
