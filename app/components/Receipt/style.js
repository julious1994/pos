const styles = {
  container: {
    background: '#eee',
  },
  section: {
    maxWidth: '1000px',
    minHeight: '1000px',
    margin: 'auto',
    border: '2px dashed #e0e0e0',
  },
  actions: {
    display: 'flex',
    padding: '10px 20px',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottom: '2px dashed #e0e0e0',
  },
  cart: {
    display: 'flex',
    flexFlow: 'column nowrap',
  },
  cartInner: {
    display: 'flex',
    justifyContent: 'center',
    margin: '20px',
  },
  printContainer: {
    maxHeight: '500px',
    overflow: 'auto',
    overflowX: 'hidden',
  },
  printReceipt: {
    color: '#212121',
    fontFamily: 'monospace',
    width: '480px',
    background: '#fff',
    padding: '10px 16px',
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'center',
  },
  printInfo: {
    marginBottom: '10px',
    marginLeft: '5px',
    display: 'flex',
    flexFlow: 'column nowrap',
  },
  printTotal: {
    marginTop: '20px',
    fontSize: '16px',
  },
};

export default styles;
