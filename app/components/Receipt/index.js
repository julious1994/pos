import React, { Component } from 'react';
import moment from 'moment';
import RaisedButton from 'material-ui/RaisedButton';

import { formatNumber } from '../../utils';
import { getCartTotal } from '../../store/reducers/cart';
import { onHome } from '../../routes';
import styles from './style';

export default class Receipt extends Component {

  nextOrder() {
    onHome();
  }

  printReceipt() {
    const div = document.getElementById('receipt').innerHTML;
    const html = `<html><body>${div}</body></html>`;
    const tab = window.open('', 'print order');
    tab.document.write(html);
    tab.print();
  }

  render() {
    const { cart } = this.props;
    const { items } = cart;
    const total = getCartTotal(items);
    let tenderTotal = 0;
    cart.payment.tenders.forEach((t) => {
      tenderTotal += Number(t.value);
    });
    const change = tenderTotal - total;

    return (
      <main style={styles.container}>
        <div style={styles.section}>
          <div style={styles.actions}>
            <div>

            </div>
            <div>
              <h1> Change :  $ {formatNumber(change)} </h1>
            </div>
            <div>
              <RaisedButton
                label="Next Order"
                primary={true}
                onTouchTap={() => this.nextOrder() }
               />
            </div>
          </div>
          <div style={styles.cart}>
            <div style={styles.cartInner}>
              <RaisedButton
                fullWidth={true}
                onTouchTap={() => this.printReceipt()}
                label="Print Receipt" />
            </div>
            <div style={styles.cartInner}>
              <div id="receipt" ref="receipt" style={styles.printContainer}>
                <div style={styles.printReceipt}>
                  <p style={{ textAlign: 'center' }}>
                    { moment(cart.created_at).format('MMMM Do YYYY, h:mm:ss a') }
                  </p>
                  <div style={styles.printInfo}>
                    <span>Axelor Company</span>
                    <span>123-456-7890</span>
                    <span>User : admin</span>
                    <span>Shop : Stock</span>
                  </div>
                  <table>
                    <tbody>
                      {
                        items.map((item, i) => ((
                          <tr key={i}>
                            <td style={{ width: '200px', textAlign: 'left' }}> {item.title }</td>
                            <td style={{ width: '120px', textAlign: 'right' }}>
                              {Number(item.qty).toFixed(3)} {item.measure}
                            </td>
                            <td style={{ width: '120px', textAlign: 'right' }}> $ { item.rate }</td>
                          </tr>
                        ))
                        )
                      }
                    </tbody>
                  </table>
                  <table style={styles.printTotal}>
                    <tbody>
                      <tr>
                        <td style={{ width: '220px', textAlign: 'left' }}> Total </td>
                        <td style={{ width: '230px', textAlign: 'right' }}>
                          $ {formatNumber(total)}
                        </td>
                      </tr>
                      <tr>
                        <td style={{ width: '220px', textAlign: 'left' }}> Change </td>
                        <td style={{ width: '230px', textAlign: 'right' }}>
                          $ {formatNumber(change)}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    );
  }
}

Receipt.propTypes = {
  cart: React.PropTypes.object,
  onInvoice: React.PropTypes.func,
};
