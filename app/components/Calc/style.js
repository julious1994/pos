export const styles = {
  calc: {
    core: {
      background: '#eee',
      padding: '20px 10px',
      display: 'flex',
      flexFlow: 'row wrap',
      justifyContent: 'center',
    },
    block1: {
      display: 'inline-flex',
      flexFlow: 'column nowrap',
      justifyContent: 'center',
      width: 140,
      marginRight: 5,
    },
    block2: {
      display: 'flex',
      flexFlow: 'row wrap',
      justifyContent: 'center',
      width: 275,
    },
    btn: {
      height: 40,
      width: 50,
      margin: 4,
      minWidth: 'auto',
    },
    extra: {
      height: 40,
      width: 65,
      margin: 4,
    },
    customer: {
      height: 40,
      margin: 4,
      width: '100%',
    },
    payment: {
      height: 140,
      width: '100%',
      margin: 4,
    },
  },
  display: {
    fontSize: 20,
    color: '#555',
    fontWeight: 'bold',
  },
};

export default styles;
