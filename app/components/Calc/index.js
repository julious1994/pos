import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import DoneAll from 'material-ui/svg-icons/action/card-giftcard';
import PersonOutline from 'material-ui/svg-icons/social/person-outline';
import Backspace from 'material-ui/svg-icons/content/backspace';
import RaisedButton from 'material-ui/RaisedButton';

import * as ROUTES from '../../routes';
import styles from './style';

export const Calc = ({ pressKey, calc, customers, selectedCustomer }) => {
  const customer = customers.filter(c => c.id === selectedCustomer);
  const btns = [
    { label: '1' }, { label: '2' }, { label: '3' }, { label: 'Qty', feature: true },
    { label: '4' }, { label: '5' }, { label: '6' }, { label: 'Disc', feature: true },
    { label: '7' }, { label: '8' }, { label: '9' }, { label: 'Rate', feature: true },
  ];
  return (
    <div className="calc" style={styles.calc.core}>
      <div style={styles.calc.block1}>
        <Link to={ROUTES.CUSTOMER}>
          <RaisedButton className="btn textual" label="Customer" labelPosition="after"
            icon={<PersonOutline/>} style={styles.calc.customer} />
        </Link>
        <Link to={ROUTES.PAYMENT}>
          <RaisedButton className="btn textual" label="Payment" labelPosition="after"
          icon={<DoneAll />} style={styles.calc.payment} />
        </Link>
      </div>
      <div style={styles.calc.block2} >
        {
          btns.map(({ label, feature }, i) => (
            <RaisedButton
              key={i}
              className="btn"
              label={label}
              primary={feature && calc.feature === label.toLowerCase()}
              style={feature ? styles.calc.extra : styles.calc.btn}
              onTouchTap={() => pressKey(label.toLowerCase()) }
              />
          ))
        }
        <RaisedButton
          className="btn"
          label="+/-"
          style={styles.calc.btn}
          secondary={calc.impact === '-'}
          onTouchTap={() => pressKey('+/-') }
          />
        <RaisedButton
          className="btn"
          label="0"
          style={styles.calc.btn}
          onTouchTap={() => pressKey('0') }
          />
        <RaisedButton
          className="btn"
          label="."
          style={styles.calc.btn}
          onTouchTap={() => pressKey('.') }
          />
        <RaisedButton
          className="btn"
          label=""
          icon={<Backspace />}
          primary={calc.feature === 'del'}
          style={styles.calc.extra}
          onTouchTap={() => pressKey('del') }
          />
      </div>
      {customer.length > 0 &&
        <p style={styles.display}> Ordered By: {customer[0].name}</p>
      }
    </div>
  );
};

Calc.propTypes = {
  calc: PropTypes.any,
  selectedCustomer: PropTypes.any,
  customers: PropTypes.array,
  pressKey: PropTypes.func,
};

Calc.defaultProps = {
  customers: [],
  pressKey: (key) => {
    console.log('pressed key', key);
  },
};

export default Calc;
