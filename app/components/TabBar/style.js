export const style = {
  tab: {
    margin: 5,
    width: 60,
    minWidth: 'auto',
  },
  tabs: {
    marginTop: '10px',
  },
  tabActions: {
    position: 'fixed',
    right: '2%',
    bottom: '2%',
    zIndex: 999999,
  },
};

export default style;
