import React, { Component } from 'react';
import style from './style';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

export default class TabBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  closeTab() {
    const { tabs, selectedTab, onRemoveTab } = this.props;
    const selectedCart = tabs[selectedTab];

    if (selectedCart.items.length > 0) {
      this.setState({ open: true });
    } else {
      onRemoveTab();
    }
  }

  handleCloseYes() {
    this.props.onRemoveTab();
    this.setState({ open: false });
  }

  handleClose() {
    this.setState({ open: false });
  }

  render() {
    const actions = [
      <FlatButton
        label="NO"
        primary={true}
        onTouchTap={() => this.handleClose()}
        />,
      <FlatButton
        label="YES"
        primary={true}
        keyboardFocused={true}
        onTouchTap={() => this.handleCloseYes()}
        />,
    ];
    const { onAddTab, onSelect, selectedTab, tabs } = this.props;

    return (
      <div className="tabs-container">
        <Dialog
          title="Shopping Cart"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose.bind(this) }
          >
          Shopping Cart contains Items..Sure Delete?
        </Dialog>

        <div style={style.tabActions}>
          <FloatingActionButton
            onTouchTap={() => onAddTab() }
            >
            <ContentAdd />
          </FloatingActionButton>

          <FloatingActionButton
            disabled={tabs.length === 1}
            secondary={true}
            onTouchTap={this.closeTab.bind(this) }
            >
            <ContentRemove />
          </FloatingActionButton>
        </div>

        <div className="tabs" style={style.tabs}>
          {tabs.map((item, i) =>
            <RaisedButton
              key={i}
              onTouchTap={() => onSelect(i)}
              label={<span>{item.tabIndex}</span>}
              primary={i === selectedTab}
              style={style.tab}
              />
          )}
        </div>
      </div>
    );
  }
}

TabBar.propTypes = {
  tabs: React.PropTypes.array,
  selectedTab: React.PropTypes.number,
  onSelect: React.PropTypes.func,
  onAddTab: React.PropTypes.func,
  onRemoveTab: React.PropTypes.func,
};

TabBar.defaultProps = {
  tabs: [],
  selectedTab: 0,
  onSelect: (v) => {
    console.log('select tab', v);
  },
  onAddTab: () => {
    console.log('add tab');
  },
  onRemoveTab: () => {
    console.log('remove tab');
  },
};
