import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';

import styles from './style';
import store from '../../store/index';
import * as ACTION from '../../store/actions';

export default class ProductBar extends Component {
  constructor(props) {
    super(props);
    const { category, sort, search } = this.props.filters;
    this.state = {
      category,
      sort,
      search,
      open: false,
    };
  }

  handleChange(event, index, category) {
    this.setState({ category });
    this.props.onCategoryFilter(category);
  }

  handleSorting(event, sort) {
    this.setState({ sort });
    this.props.onSort(sort);
  }

  handleSearching(event, search) {
    this.setState({ search });
    store.dispatch(
      ACTION.fetchProducts(
        {
          criteria: [
            { fieldName: 'fullName', operator: 'like', value: search },
          ],
          operator: 'or',
        }
      )
    );
    // this.props.onSearch(search);
  }


  render() {
    const { category, search, sort } = this.state;
    return (
      <div className="tabs-container" style={{ overflow: 'hidden' }}>
        <Toolbar style={styles.toolbar}>
          <ToolbarGroup firstChild={true} >
            <DropDownMenu value={category} onChange={(e, i, v) => this.handleChange(e, i, v)}>
              <MenuItem value={0} primaryText="All Products" />
              {this.props.categories.map((cat, i) =>
                  <MenuItem
                    key={i}
                    value={cat.id}
                    primaryText={cat.name}
                  />
              )}
            </DropDownMenu>
          </ToolbarGroup>
          <ToolbarGroup style={styles.searchBar}>
            { ' ' }
            <TextField
              style={{ width: '200px' }}
              hintText="Search Product"
              value={search}
              onChange={(e, v) => this.handleSearching(e, v)}
            />
            <IconMenu
              iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
              onChange={(e, v) => this.handleSorting(e, v)}
              value={sort}
              >
              <MenuItem value="name" primaryText="Sort By Name" />
              <MenuItem value="rate" primaryText="Sort By Price" />
              <MenuItem value="category" primaryText="Sort By Category" />
          </IconMenu>

          </ToolbarGroup>
        </Toolbar>
      </div>
    );
  }
}

ProductBar.propTypes = {
  title: React.PropTypes.string,
  categories: React.PropTypes.array,
  filters: React.PropTypes.any,
  onCategoryFilter: React.PropTypes.func,
  onSort: React.PropTypes.func,
  onSearch: React.PropTypes.func,
};

ProductBar.defaultProps = {
  title: 'Product Bar',
  categories: [],
  filters: {
    category: 0,
    search: '',
    sort: 'id',
  },
  onCategoryFilter: (v) => {
    console.log('filter category', v);
  },
  onSort: (v) => {
    console.log('filter sort', v);
  },
  onSearch: (v) => {
    console.log('filter search', v);
  },
};
