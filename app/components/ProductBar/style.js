export const styles = {
  toolbar: {
    flexFlow: 'row wrap',
    height: 'auto',
    overflow: 'hidden',
  },
  searchBar: {
    flexFlow: 'row wrap',
  },
};

export default styles;
