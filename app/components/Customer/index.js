import React, { Component } from 'react';
import { onHome, onPayment } from '../../routes';
import styles from './style';
import AddCustomer from './AddCustomer';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn } from 'material-ui/Table';
import TextField from 'material-ui/TextField';

const _ = require('lodash');

export default class Customer extends Component {
  constructor(props) {
    super(props);
    const { selectedCustomer } = this.props;
    this.sort = '';
    this.state = {
      selected: selectedCustomer,
      deselect: selectedCustomer !== 0,
      show: false,
      customer: this.initCust(),
    };
  }

  initCust() {
    return {
      id: 0,
      name: '',
      phone: '',
      address: '',
    };
  }

  addNew() {
    this.setState({ show: true });
  }

  edit(customer) {
    this.setState({ show: true, customer });
  }

  save(data) {
    const { id } = data;
    const { onAdd, onUpdate } = this.props;

    if (id === 0) {
      onAdd(data);
    } else {
      onUpdate(id, data);
    }
    this.cancle();
  }

  cancle() {
    this.setState({ show: false, customer: this.initCust() });
  }

  remove(id) {
    this.props.onDelete(id);
  }

  doSelectCustomer(customer) {
    const { id } = customer;
    const deselect = this.props.selectedCustomer === customer.id;
    this.setState({ selected: id, deselect, customer });
  }

  componentWillReceieveProps(nextProps) {
    this.setState({ selected: nextProps.selectedCustomer });
  }

  setCustomer() {
    const { onSelection, onSelectionRemove } = this.props;
    if (this.state.deselect === true) {
      onSelectionRemove();
    } else {
      onSelection(this.state.selected);
    }
    this.cancel();
  }

  cancel() {
    const { redirect } = this.props.location.query;
    if (redirect === 'payment') {
      onPayment();
    } else {
      onHome();
    }
  }

  filterCustomers() {
    const { filters, customer } = this.props;
    const { search, sort, asc } = filters;
    let items = customer.items;

    if (search) {
      items = items.filter(item =>
        item.name.toLowerCase().indexOf(search.toLowerCase()) > -1);
    }
    if (sort) {
      items = _.sortBy(items, sort, (n) => Math.sin(n));
      if (asc === false) {
        items = items.reverse();
      }
    }

    return items;
  }

  renderCustomerAction() {
    let act = '';
    if (this.state.deselect === true) {
      act = 'Deselect Customer';
    } else {
      act = this.props.selectedCustomer === 0 ? 'Set Customer' : 'Change Customer';
    }
    return act;
  }

  renderCustomers(customers, selected) {
    const { onSort } = this.props;

    return (
      <Table>
        <TableHeader>
          <TableRow>
            <TableHeaderColumn onTouchTap={() => onSort('name')}>Name</TableHeaderColumn>
            <TableHeaderColumn onTouchTap={() => onSort('address')}>Address</TableHeaderColumn>
            <TableHeaderColumn onTouchTap={() => onSort('phone')}>Phone</TableHeaderColumn>
            <TableHeaderColumn style={{ width: '40px' }}></TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody deselectOnClickaway={false} showRowHover={true}>
          {customers.map((cust, i) =>
              <TableRow
                key={i}
                selectable={false}
                selected={selected === cust.id}
                onTouchTap={() => this.doSelectCustomer(cust)}>
                  <TableRowColumn>{cust.name}</TableRowColumn>
                  <TableRowColumn>{cust.address}</TableRowColumn>
                  <TableRowColumn>{cust.phone}</TableRowColumn>
                  <TableRowColumn style={{ width: '40px' }}>
                    <IconMenu
                      iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
                      anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                      targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                    >
                      <MenuItem primaryText="Edit"
                        onTouchTap={() => this.edit(cust)}
                      />
                      <MenuItem primaryText="Remove"
                        onTouchTap={() => this.remove(cust.id)}
                      />
                    </IconMenu>
                  </TableRowColumn>
              </TableRow>
          )}
        </TableBody>
      </Table>
    );
  }

  render() {
    const { show, deselect, selected } = this.state;
    const { customer, onSearch } = this.props;
    const selectedOne = customer.items.filter(i => i.id === selected);
    const customers = this.filterCustomers();
    return (
      <main style={styles.container}>
        <div style={styles.section}>
          <div style={styles.actions}>
            <div>
              <RaisedButton label="Cancel" onTouchTap={() => this.cancel()} />
            </div>
            <div>
              <TextField
                floatingLabelText="Search customers"
                onChange={(e) => onSearch(e.target.value)}
              />
            </div>
            <div>
              <RaisedButton
                label={ this.renderCustomerAction() }
                primary={!deselect}
                secondary={deselect}
                onTouchTap={() => this.setCustomer()}
              />
            </div>
          </div>
          <div >
              {
                show === true &&
                <AddCustomer
                  onCancel={() => this.cancle()}
                  onSave={(data) => this.save(data)}
                  customer={this.state.customer}
                />
              }

              {selectedOne.length > 0 && show === false &&
                <div style={styles.profile}>
                  <h1>{selectedOne[0].name}</h1>
                  <p><b>Address :</b> {selectedOne[0].address}</p>
                  <p><b>Contact :</b> {selectedOne[0].phone}</p>
                </div>
              }
          </div>

          <div style={styles.table}>
            {this.renderCustomers(customers, selected)}
          </div>
        </div>
        <div style={styles.actionBar}>
          <FloatingActionButton
            onTouchTap={() => this.addNew()}
            >
            <ContentAdd />
          </FloatingActionButton>
        </div>
      </main>
    );
  }
}

Customer.propTypes = {
  selectedCustomer: React.PropTypes.number,
  customer: React.PropTypes.any,
  filters: React.PropTypes.any,
  location: React.PropTypes.any,
  onSelection: React.PropTypes.func,
  onSelectionRemove: React.PropTypes.func,
  onAdd: React.PropTypes.func,
  onUpdate: React.PropTypes.func,
  onDelete: React.PropTypes.func,
  onSort: React.PropTypes.func,
  onSearch: React.PropTypes.func,
};
