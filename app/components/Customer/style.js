export const styles = {
  container: {
    background: '#eee',
  },
  section: {
    maxWidth: '1000px',
    paddingBottom: '50px',
    margin: 'auto',
    border: '2px dashed #e0e0e0',
    position: 'relative',
  },
  actions: {
    display: 'flex',
    padding: '10px 20px',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottom: '2px dashed #e0e0e0',
  },
  table: {
    height: '610px',
    overflow: 'auto',
  },
  profile: {
    padding: '10px',
    borderBottom: '2px dashed #e0e0e0',
  },
  actionBar: {
    position: 'fixed',
    bottom: '2%',
    right: '2%',
  },
};

export default styles;
