import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

const style = {
  container: {
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'space-around',
    padding: '10px 4%',
  },
  basic: {
    display: 'flex',
    flexFlow: 'column nowrap',
  },
  actions: {
    display: 'flex',
    flexFlow: 'column wrap',
    justifyContent: 'center',
  },
};

export default class AddCustomer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customer: this.props.customer,
    };
  }

  componentWillReceiveProps(nextProp) {
    const { customer } = nextProp;
    this.setState({ customer });
  }

  handleInput(field, e) {
    const { customer } = this.state;
    customer[field] = e.target.value;
    this.setState({ customer });
  }

  render() {
    const { onCancel, onSave } = this.props;
    const { customer } = this.state;

    return (
      <form>
        <div style={style.container}>
          <div style={style.basic}>
            <div>
              <TextField
                style= {{ marginRight: '10px' }}
                floatingLabelText="Name"
                value={customer.name}
                onChange={(e) => this.handleInput('name', e)}
              />
              <TextField
                floatingLabelText="Contact"
                value={customer.phone}
                onChange={(e) => this.handleInput('phone', e)}
              />
            </div>
            <div>
              <TextField
                floatingLabelText="Address"
                value={customer.address}
                onChange={(e) => this.handleInput('address', e)}
              />
            </div>
          </div>
          <div style={style.actions}>
            <RaisedButton label="save" primary={true}
              onTouchTap={() => onSave(customer)}
             />
            <br/>
            <RaisedButton label="cancel" secondary={true}
              onTouchTap={() => onCancel()}
             />
          </div>
        </div>
      </form>
    );
  }
}

AddCustomer.propTypes = {
  onCancel: React.PropTypes.func.isRequired,
  onSave: React.PropTypes.func.isRequired,
  customer: React.PropTypes.object,
};
