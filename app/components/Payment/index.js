import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import RemoveCircle from 'material-ui/svg-icons/content/remove-circle';
import Backspace from 'material-ui/svg-icons/content/backspace';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn } from 'material-ui/Table';
import Dialog from 'material-ui/Dialog';

import { formatNumber } from '../../utils';
import { onHome, onCustomer, onReceipt } from '../../routes';
import { getCartTotal } from '../../store/reducers/cart';
import styles from './style';

export default class Payment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
    };
  }

  validatePayment(cust) {
    if (cust === 0 || cust === undefined) {
      this.setState({ show: true });
    } else {
      onReceipt();
    }
  }

  confirmCustomer() {
    onCustomer('redirect=payment');
  }

  handleClose() {
    this.setState({ show: false });
  }

  renderTenders(total) {
    const { cart, onSelect, onRemove } = this.props;
    const { payment } = cart;
    const { tenders, selectedTender } = payment;
    let remain = total;
    let lastTender = 0;
    let change = 0;
    let tender = 0;

    return (
      <div style={styles.paymentBlock}>
        { tenders.length === 0 &&
          <div style={styles.paymentNotice}>
            <span style={styles.title}>{`$ ${formatNumber(total)}`}</span>
            <p> please select payment method </p>
          </div>
        }
        <div style={styles.table}>
          { tenders.length > 0 &&
            <Table selectable={false}>
              <TableHeader adjustForCheckbox={false}
                displaySelectAll={false}>
                <TableRow>
                  <TableHeaderColumn style={styles.th}>Due</TableHeaderColumn>
                  <TableHeaderColumn style={styles.th}>Tendered</TableHeaderColumn>
                  <TableHeaderColumn style={styles.th}>Change</TableHeaderColumn>
                  <TableHeaderColumn style={styles.th}>Method</TableHeaderColumn>
                  <TableHeaderColumn style={{ width: '30px' }}></TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false}>
                {
                  tenders.map((tenderObj, i) => {
                    tender = tenderObj.value;
                    remain -= lastTender;
                    lastTender = Number(tender);
                    change = tender - remain;

                    return (
                      <TableRow
                        key={i}
                        className={ i === selectedTender ? 'tender-selected' : ''}
                        onTouchTap={() => onSelect(i) }
                        >
                        <TableRowColumn style={styles.td}>
                          {formatNumber(remain > 0 ? remain : 0) }
                        </TableRowColumn>
                        <TableRowColumn style={styles.td}>
                          <input
                            type="text"
                            value={formatNumber(tender) }
                            readOnly={true}
                            style={styles.tenderInput}
                            />
                        </TableRowColumn>
                        <TableRowColumn style={styles.td} className={change > 0 ? 'change' : ''}>
                          {change > 0 ? formatNumber(change) : ''}
                        </TableRowColumn>
                        <TableRowColumn style={styles.td}>
                          {tenderObj.method}
                        </TableRowColumn>
                        <TableRowColumn style={{ width: '30px' }}>
                          <RemoveCircle
                            onTouchTap={() => onRemove(i) }
                            />
                        </TableRowColumn>
                      </TableRow>
                    );
                  })
                }
                {
                tender != 0 && (remain - lastTender) > 0 &&
                <TableRow>
                    <TableRowColumn style={styles.td}>
                      {formatNumber(remain - lastTender) }
                    </TableRowColumn>
                </TableRow>
                }
              </TableBody>
            </Table>
          }
        </div>
      </div>
    );
  }

  renderCalc() {
    const { onCalcPress } = this.props;
    const keys = ['1', '2', '3', '+10', '4', '5', '6', '+20', '7', '8', '9', '+50', 'C', '0', '.'];
    return (
      <div className="calc" style={styles.calcContainer}>
        {
          keys.map((key, i) =>
            <RaisedButton
              key={i}
              label={key}
              style={styles.calcBtn}
              onTouchTap={() => onCalcPress(key) }
              />
          ) }
        <RaisedButton
          key='DEL'
          icon={<Backspace />}
          style={styles.calcBtn}
          onTouchTap={() => onCalcPress('DEL') }
          />
      </div>
    );
  }

  render() {
    const { customers, cart, onAdd, onInvoice } = this.props;
    const total = getCartTotal(cart.items);
    let tenderTotal = 0;
    cart.payment.tenders.forEach((t) => {
      tenderTotal += Number(t.value);
    });
    const customer = customers.filter(c => c.id === cart.selectedCustomer);

    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={() => this.handleClose()}
      />,
      <FlatButton
        label="Confirm"
        primary={true}
        keyboardFocused={true}
        onTouchTap={() => this.confirmCustomer()}
      />,
    ];

    return (
      <main style={styles.container}>
        <div style={styles.section}>
          <div style={styles.actions}>
            <div>
              <RaisedButton label="Go Back" onTouchTap={() => onHome() } />
            </div>
            <div>
              <h1> Payment </h1>
            </div>
            <div>
              <RaisedButton
                label="Validate"
                primary={true}
                disabled={tenderTotal < total}
                onTouchTap={() => this.validatePayment(cart.selectedCustomer) }
               />
               <Dialog
                title="Please Select Customer"
                actions={actions}
                modal={false}
                open={this.state.show}
                onRequestClose={() => this.handleClose}
                >
                You need to select customer before you can invoice an order.
              </Dialog>
            </div>
          </div>
          <div style={styles.payment}>
            <RaisedButton label="cash"
              style={styles.cashButton}
              backgroundColor="#009688"
              labelColor="#fff"
              onTouchTap={() => onAdd(0, 'CASH (USD)') }
              />

            {this.renderTenders(total) }

          </div>
          <div style={styles.calc}>

            {this.renderCalc() }

            <div style={styles.options}>
              <RaisedButton label={customer.length > 0 ? customer[0].name : 'CUSTOMER'}
                style={styles.optionBtn}
                onTouchTap={() => onCustomer('redirect=payment') }
                />
              <RaisedButton label="INVOICE"
                style={styles.optionBtn}
                primary={cart.payment.invoice}
                onTouchTap={() => onInvoice() }
                />
            </div>
          </div>
        </div>
      </main>
    );
  }
}

Payment.propTypes = {
  cart: React.PropTypes.object,
  customers: React.PropTypes.array,
  onAdd: React.PropTypes.func,
  onUpdate: React.PropTypes.func,
  onRemove: React.PropTypes.func,
  onSelect: React.PropTypes.func,
  onCalcPress: React.PropTypes.func,
  onInvoice: React.PropTypes.func,
};
