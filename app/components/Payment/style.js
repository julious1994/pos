const styles = {
  container: {
    background: '#eee',
  },
  section: {
    maxWidth: '1000px',
    minHeight: '1000px',
    margin: 'auto',
    border: '2px dashed #e0e0e0',
  },
  actions: {
    display: 'flex',
    padding: '10px 20px',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottom: '2px dashed #e0e0e0',
  },
  payment: {
    display: 'flex',
    flexFlow: 'row wrap',
  },
  paymentNotice: {
    display: 'flex',
    flexFlow: 'column nowrap',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paymentBlock: {
    width: '100%',
    overflow: 'auto',
    maxHeight: '480px',
  },
  title: {
    color: '#009688',
    fontSize: '50px',
    fontWeight: 'bold',
  },
  cashButton: {
    height: 50,
    width: 150,
    margin: 20,
    alignItems: 'center',
  },
  table: {
    overflow: 'hidden',
  },
  th: {
    fontWeight: 'bold',
    fontSize: '20px',
  },
  td: {
    fontSize: '18px',
    position: 'relative',
    fontWeight: 'bold',
  },
  tenderInput: {
    height: '100%',
    width: '100%',
    borderRadius: '4px',
    border: 'none',
    background: '#fff',
    fontSize: '22px',
    padding: '0 10px',
    color: '#009688',
  },
  calc: {
    marginTop: '20px',
    display: 'flex',
  },
  calcContainer: {
    border: '2px dashed #e0e0e0',
    margin: '25px',
    width: '325px',
    display: 'flex',
    flexFlow: 'row wrap',
  },
  calcBtn: {
    height: 45,
    width: 70,
    fontSize: '18px',
    margin: '4px',
    minWidth: 'auto',
  },
  options: {
    display: 'flex',
    flexFlow: 'column wrap',
    marginTop: '25px',
  },
  optionBtn: {
    minWidth: 160,
    height: 60,
    margin: 10,
  },
};

export default styles;
