import React, { Component, PropTypes } from 'react';
import { ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';

import { formatNumber } from '../../utils';
import { getCartItemStats } from '../../store/reducers/cart';

export default class CartItem extends Component {
  render() {
    const { selected, title, thumb, measure, onSelect } = this.props;
    const { qty, disc, rate, net } = getCartItemStats(this.props);
    return (
      <ListItem
        className={selected === true ? 'item-selected' : ''}
        onTouchTap={() => {
          if (selected === false) {
            onSelect(this.props);
          }
        }}
        leftAvatar={<Avatar style={{ backgroundColor: 'transparent' }} src={thumb} />}
        primaryText={
          <p style={{ margin: '0px' }}>
            { title }
            <span style={{ float: 'right' }}>
              {`$ ${formatNumber(net)}`}
            </span>
          </p>
        }
        secondaryTextLines={Number(disc) > 0 ? 2 : 1}
        secondaryText={
          <p style={{ margin: '0px' }}>
            <span style={{ color: 'darkBlack' }}>
              { formatNumber(qty, 3) } { measure } {' '}
            </span>
            at $ { formatNumber(rate) } / {measure} <br/>
            { Number(disc) > 0 ? <span>With discount {disc}% </span> : ''}
          </p>
        }
        />
    );
  }
}
CartItem.propTypes = {
  onSelect: PropTypes.func.isRequired,
  title: PropTypes.string,
  thumb: PropTypes.string,
  measure: PropTypes.string,
  qty: PropTypes.any,
  rate: PropTypes.any,
  disc: PropTypes.any,
  paid: PropTypes.number,
  product: PropTypes.any,
  selected: PropTypes.bool,
};

CartItem.defaultProps = {
  qty: 1,
  disc: 0,
  selected: false,
  product: {
    thumb: 'img/default.png',
    name: 'item',
    measure: '',
  },
};
