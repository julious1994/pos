export function formatNumber(n, prec = 2) {
  return Number(n).toFixed(prec).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

export function computeNumber(options) {
  const { initial, key, impact, q } = options;
  let newQ;

  if (initial === false) {
    newQ = key !== '.' ? key : '0';
  } else {
    if (key === '.') {
      newQ = `${Math.floor(Number(q))}.`;
    } else {
      newQ = `${q ? q.toString() : ''}${key}`;
    }
  }

  if (impact === '-') {
    newQ = -Math.abs(newQ);
  }
  return newQ;
}

export function stripValue(q) {
  let qq = q.toString().substr(0, q.toString().length - 1);
  if (qq === '-') {
    qq = '0';
  }
  return q !== '0' && qq.length === 0 ? '0' : qq;
}
