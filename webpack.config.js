/* eslint-disable */
const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const loaders = require('./webpack.loaders');
const outputPath = path.join(__dirname, './dist');

const config = {
  entry: [
    'webpack-dev-server/client?http://0.0.0.0:3034',
    'webpack/hot/only-dev-server',
    './app/index.js',
  ],
  devtool: process.env.WEBPACK_DEVTOOL || 'eval-source-map',
  output: {
    path: outputPath,
    filename: 'bundle.js'
  },
  node: {
    console: true,
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: loaders,
    noParse: /node_modules\/json-schema\/lib\/validate\.js/
  },
  devServer: {
    contentBase: outputPath,
    noInfo: true,
    hot: true,
    inline: true,
    historyApiFallback: true,
    port: 3034,
    host: '0.0.0.0'
  },
  plugins: [
    new webpack.NoErrorsPlugin(),
    new CopyWebpackPlugin([{ from: './public' }], { copyUnmodified: true }),
  ],
  externals: [
    // put your node 3rd party libraries which can't be built with webpack here
    // (mysql, mongodb, and so on..)
  ]
};

module.exports = config;

